const navbar = document.querySelector('.my-navbar');
const toggler = navbar.querySelector('.my-toggler');
toggler.collapse = navbar.querySelector(toggler.getAttribute('my-data-target'));

function toggleMenu(e){
  if(e.target.classList.contains('collapsed')){
    e.target.classList.remove('collapsed');
    e.target.collapse.classList.remove('collapsed');
  }else{
    e.target.classList.add('collapsed');
    e.target.collapse.classList.add('collapsed');
  }
}

toggler.addEventListener('click', toggleMenu);