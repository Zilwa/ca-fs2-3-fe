// Lambda funkcijos, vyksta užsikrovus visam puslapio turiniui,
// todėl kodą įdedami į lambda išraiką ir iškart juą iškviesdami užtikriname, jog
// kodas įvyks tik užsikrovus turiniui.
(() => { 
  const
    navbar = document.querySelector('.navbar'),
    toggler = document.querySelector('.navbar-toggler'),
    links = document.querySelectorAll('.collapse li.nav-item'),
    breakpoints = {
      xs: 0,
      sm: 576,
      md: 768,
      lg: 992,
      xl: 1200,
    },
    className = Array.from(navbar.classList)
      .find(className => /navbar-expand-(.*)/.test(className)),
    breakpoint = className
      ? breakpoints[className.match(/navbar-expand-(.*)/)[1]]
      : Number.MAX_SAFE_INTEGER;

  links.forEach(link => link.addEventListener('click', () =>
    window.innerWidth < breakpoint ? toggler.click() : null
  ));
})();
