export default class PrintService {
  constructor(text) {
    this.text = text;
    this.element = document.createElement('div');
  }

  update(text) {
    this.element.innerHTML = text;
  }

  render() {
    this.element.innerHTML = this.text;
    this.element.style.width = '200px';
    this.element.style.border = '1px solid #000';
    this.element.style.padding = '1rem';
    return this.element;
  }
}