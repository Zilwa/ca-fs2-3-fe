import Person from './entities/Person';
import InputService from './services/InputService';
import PrintService from './services/PrintService';
import './scss/main.scss';   

const root = document.querySelector('#app');

const inputService = new InputService(({name, surname, writeCase}) => {  
  const person = new Person(name, surname);
  // 1. Paprastas sprendimo variantas
  let methodName, fullname;
  switch (writeCase) {
    case 'camel': methodName = 'getFullnameCamel'; break;
    case 'kebab': methodName = 'getFullnameKebab'; break;
    case 'pascal': methodName = 'getFullnamePascal'; break;
    case 'snake': methodName = 'getFullnameSnake'; break;
  }
  if(methodName) fullname = person[methodName]();
  else fullname = person.fullname;

  printService.update(fullname); 
});

const printService = new PrintService('Cia bus jusu reklama');

root.appendChild(inputService.render());
root.appendChild(printService.render());

