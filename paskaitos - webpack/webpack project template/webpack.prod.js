const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackCommon = require('./webpack.common');

module.exports = merge(webpackCommon, {
  mode: 'production',
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/template.html',
      templateParameters: {
        'title': 'Production'
      }
    }),
  ]
});