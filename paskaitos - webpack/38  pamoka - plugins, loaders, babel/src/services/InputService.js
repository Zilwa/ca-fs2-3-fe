export default class InputService {
  constructor(callback) {
    this.callback = callback;
    this.element = document.createElement('form');
    this.element.addEventListener('submit', this.onSubmit);
    // this.onSubmit = this.onSubmit.bind(this);
  }

  // onSubmit(e) {
  onSubmit = (e) => {
    e.preventDefault();
    const data = Array.from(this.element)
      .filter(input => input.tagName !== 'BUTTON')
      .reduce((ob, input) => {
        return {
          ...ob,
          [input.name]: input.value
        }
      }, {});
    this.callback(data);
    this.render();
  }

  render() {
    this.element.innerHTML = `
      <div>
        <label for="name">Name</label>
        <input type="text" id="name" name="name" />
      </div>
      <div>
        <label for="surname">Surname</label>
        <input type="text" id="surname" name="surname" />
      </div>
      <div>
        <button>Print full name in selected case</button>
      </div>
    `;
    return this.element;
  }
}
