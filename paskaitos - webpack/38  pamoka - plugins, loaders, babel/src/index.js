import Person from './entities/Person';
import InputService from './services/InputService';
import './scss/main.scss';   

const root = document.querySelector('#app');

const inputService = new InputService((data) => {  
  console.log(data);
});
root.appendChild(inputService.render());

