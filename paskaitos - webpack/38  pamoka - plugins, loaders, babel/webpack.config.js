const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');


module.exports = {
  entry: './src/index.js',
  mode: 'development',
  output: {
    filename: 'main.[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    open: 'chrome'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              "@babel/plugin-syntax-dynamic-import",
              "@babel/plugin-proposal-class-properties"
            ]
          },
        }
      },
      {
        test: /\.scss$/i,
        exclude: /node_modules/,
        use: [
          // 3. Perkeliama į failą, pagal MiniCssExtractPlugin plugin'o nustatymus
          MiniCssExtractPlugin.loader,
          // 2. s[ac]ss paverčiamas į css
          'css-loader',
          // 1. Galite importuoti .s[ac]ss failus Javascripté. Nuskaitomi s[ac]ss failai.
          'sass-loader'
        ],
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: './src/template.html',
      templateParameters: {
        'title': 'Development'
      }
    }),
    new MiniCssExtractPlugin({
      linkType: 'text/css',
      filename: '[name].[contenthash].css',
    })
  ]
};