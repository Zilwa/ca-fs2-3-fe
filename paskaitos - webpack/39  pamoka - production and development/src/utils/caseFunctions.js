const readCaseToCamelCase = str => {
  let result = str[0].toLowerCase();
  str = str.slice(1);
  let nextLetterCapital = false;
  for (let i = 0; i < str.length; i++) {
    const char = str[i];
    if (char === ' ') nextLetterCapital = true;
    else {
      result += nextLetterCapital ? char.toUpperCase() : char.toLowerCase();
      nextLetterCapital = false;
    }
  }
  return result;
}

const readCaseToKebabCase = str => {
  for (let i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) === 32) {
      str = str.slice(0, i) + '-' + str.slice(i + 1);
    }
  }
  return str.toLowerCase();
}


const readCaseToPascalCase = str => {
  // const camelCase = readCaseToCamelCase(str);
  // return camelCase[0].toUpperCase() + camelCase.slice(1);
  let result = str[0].toUpperCase();
  str = str.slice(1);
  let nextLetterCapital = false;
  for (let i = 0; i < str.length; i++) {
    const char = str[i];
    if (char === ' ') nextLetterCapital = true;
    else {
      result += nextLetterCapital ? char.toUpperCase() : char.toLowerCase();
      nextLetterCapital = false;
    }
  }
  return result;
}

const readCaseToSnakeCase = str => {
  for (let i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) === 32) {
      str = str.slice(0, i) + '_' + str.slice(i + 1);
    }
  }
  return str.toLowerCase();;
}

export {
  readCaseToCamelCase,
  readCaseToKebabCase,
  readCaseToPascalCase,
  readCaseToSnakeCase
};