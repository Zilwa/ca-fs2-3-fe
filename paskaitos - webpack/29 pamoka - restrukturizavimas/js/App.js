class App {
  constructor(){
    this.gameSetup = GameSetupService.promptSettings();
    this.wordService = new WordService('.js-word-container', this.gameSetup.word);
    this.wordService.revealLetter('o');
  }
}

new App();