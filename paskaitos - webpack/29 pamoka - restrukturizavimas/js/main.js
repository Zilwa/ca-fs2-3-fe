const word = prompt('Įveskite žodį:').toLocaleLowerCase();
const lives = 7;
let livesLeft = lives;
let lettersCorrect = 0;

const wordContainer = document.querySelector('.word-container');
const mistakesContainer = document.querySelector('.mistakes-container');
const keyboardLetterObjects = Array
  .from(document.querySelectorAll('.keyboard__key'))
  .map(keyEl => { 
    const letter = keyEl.innerHTML;
    keyEl.addEventListener('click', handleLetterGuess, { once: true });
    keyEl.letter = letter;
    return {
      letter,
      correct: 'unknown', 
      keyboardKey: keyEl 
    }
  });
const wordLetterObjects = [];
const mistakeSquares = [];

for (let i = 0; i < word.length; i++) {
  const letter = word.charAt(i);
  const letterContainer = document.createElement('span');
  letterContainer.className = 'unknown-letter';
  letterContainer.innerHTML = letter;
  wordLetterObjects.push({ letter, letterContainer });
  wordContainer.appendChild(letterContainer);
}

for (let i = 0; i < lives; i++) {
  const mistakeSquare = document.createElement('span');
  mistakeSquare.className = 'mistake-square';
  mistakeSquares.push(mistakeSquare);
  mistakesContainer.appendChild(mistakeSquare);
}

function handleLetterGuess({ target }) {
  quessLetter(target.letter);
}

function quessLetter(letter) {
  const ii = keyboardLetterObjects.findIndex(keyOb => keyOb.letter === letter);
  if (word.includes(letter)) {
    keyboardLetterObjects[ii].correct = true;
    keyboardLetterObjects[ii].keyboardKey.classList.add('keyboard__key--good');
    wordLetterObjects
      .filter(wordLetterOb => wordLetterOb.letter === letter)
      .map(wordLetterOb => wordLetterOb.letterContainer)
      .forEach(wordLetterContainer => {
        lettersCorrect++;
        wordLetterContainer.classList.add('unknown-letter--answered')
      });
    if (lettersCorrect === word.length) winGame();
  } else {
    keyboardLetterObjects[ii].correct = false;
    keyboardLetterObjects[ii].keyboardKey.classList.add('keyboard__key--bad');
    mistakeSquares[lives - livesLeft--].classList.add('mistake-square--bad');
    if (livesLeft === 0) looseGame();
  }
}


function winGame() {
  alert('Laimejai');
  dissolveGame();
}

function looseGame() {
  alert('Pralaimejai');
  dissolveGame();
}

function dissolveGame() {
  keyboardLetterObjects.forEach(({ keyboardKey }) => keyboardKey.removeEventListener('click', handleLetterGuess))
}

quessLetter(word.charAt(0));