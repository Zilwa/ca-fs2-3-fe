class WordService {
  constructor(selector, word) {
    console.log(selector, word)
    this.wordContainer = document.querySelector(selector);
    this.wordLetterObjects = Array.from(word)
      .map(letter => {
        const letterContainer = document.createElement('span');
        letterContainer.className = 'unknown-letter';
        letterContainer.innerHTML = letter;
        this.wordContainer.appendChild(letterContainer);
        return { letter, letterContainer };
      })
  }

  revealLetter(letter) {
    this.wordLetterObjects.forEach(el => letter === el.letter
      ? el.letterContainer.classList.add('unknown-letter--answered')
      : null
    )
    // Parašyti logiką, jog pagal letter argumentą, 
    // surastų visas atitinkančias raides wordLetterObjects masyve, 
    // ir atitinkamiems jų HTML elementams uždėtų, klases - kurios
    // padaro tas raides matomas
  }
}