class GameSetupService {

  static promptSettings() {
    const word = prompt('Įveskite žodį:').toLocaleLowerCase();
    const uniqueLetters = Array.from(word)
      .reduce((acc, letter, i, arr) => arr.indexOf(letter) === i ? acc + 1 : acc, 0);
    return {
      word,
      lives: 7,
      lettersCorrect: 0,
      mistakeCount: 0,
      uniqueLetters,
      keyboard: [
        ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'],
        ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'],
        ['z', 'x', 'c', 'v', 'b', 'n', 'm']
      ]
    }
  }
}
