import {
  readCaseToCamelCase,
  readCaseToKebabCase,
  readCaseToPascalCase,
  readCaseToSnakeCase
} from '../utils/caseFunctions';

class Person {
  constructor(name, surname) {
    this.name = name;
    this.surname = surname;
    this.fullname = name + ' ' + surname;
  }

  getFullnameCamel() {
    return readCaseToCamelCase(this.fullname);
  }

  getFullnameKebab() {
    return readCaseToKebabCase(this.fullname);
  }

  getFullnamePascal() {
    return readCaseToPascalCase(this.fullname);
  }

  getFullnameSnake() {
    return readCaseToSnakeCase(this.fullname);
  }
}

export default Person;