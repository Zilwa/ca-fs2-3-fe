const kebabCaseToCamelCase = str => {
  for (let i = 0; i < str.length; i++) {
    if (str[i] === '-') {
      str = str.slice(0, i) + str[i + 1].toUpperCase() + str.slice(i + 2);
    }
  }
  return str;
}

const readCaseToCamelCase = str => {
  for (let i = 0; i < str.length; i++) {
    if (str[i] === '0') {
      str = str.slice(0, i) + str[i + 1].toUpperCase() + str.slice(i + 2);
    }
  }
  return str;
}

const readCaseToKebabCase = str => {
  for (let i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) === 32) {
      str = str.slice(0, i) + '-' + str.slice(i + 1);
    }
  }
  return str;
  // return str.split(' ').join('-');
}


const readCaseToPascalCase = str => {
  for (let i = 0; i < str.length; i++) {
    if (str[i] === '-') {
      str = str.slice(0, i) + str[i + 1].toUpperCase() + str.slice(i + 2);
    }
  }
  return str[0] + str.slice(1);
}

const readCaseToSnakeCase = str => {
  for (let i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) === 32) {
      str = str.slice(0, i) + '_' + str.slice(i + 1);
    }
  }
  return str;
}

export {
  kebabCaseToCamelCase,
  readCaseToCamelCase,
  readCaseToKebabCase,
  readCaseToPascalCase,
  readCaseToSnakeCase
};