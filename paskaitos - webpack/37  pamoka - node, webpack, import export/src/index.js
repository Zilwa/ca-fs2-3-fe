import Person from './entities/Person';
import InputService from './services/InputService';

const root = document.querySelector('#app');

const inputService = new InputService((data) => {
  console.log(data);
});

root.appendChild(inputService.render());
