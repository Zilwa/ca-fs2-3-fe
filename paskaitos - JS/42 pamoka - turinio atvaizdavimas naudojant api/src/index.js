import api from './api';
import loadingGif from './assets/img/loading.gif';
import UserCard from './components/UserCard';
import './scss/main.scss';

const
  root = document.querySelector('#app'),
  userFetchCount = 100;
let
  segment = 1,
  userCards = [],
  cardContainer;

cardContainer = document.createElement('div');
cardContainer.className = 'card-container';

const loadingAnimation = document.createElement('div');
loadingAnimation.className = 'loading-animation';
loadingAnimation.innerHTML = `<img src="${loadingGif}">`;

const initUserLoad = users => {
  root.removeChild(loadingAnimation);
  root.appendChild(cardContainer);
  mountUsers(users);
  setTimeout(() => cardContainer.classList.add('show'), 0);
}

const mountUsers = users => {
  const newUserCards = users.map(userData => new UserCard(userData));
  newUserCards.forEach(userCard => cardContainer.appendChild(userCard.mount()));
  userCards = [...userCards, ...newUserCards];
}

const stopInfiniteScroll = () => {
  const noticeEndOfUsers = document.createElement('div');
  noticeEndOfUsers.className = 'notice-end-of-users';
  noticeEndOfUsers.innerHTML = 'Daugiau vartotojų nėra';
  root.appendChild(noticeEndOfUsers);
}

const loadUsers = () => {
  root.appendChild(loadingAnimation);
  api.getUsers(users => {
    let continueInfiniteScroll = true;
    if (users.length < userFetchCount) continueInfiniteScroll = false;
    mountUsers(users);
    root.removeChild(loadingAnimation);
    segment++;

    if (continueInfiniteScroll) window.addEventListener('scroll', checkPageScroll);
    else stopInfiniteScroll();
  }, segment * userFetchCount);
}

const checkPageScroll = () => {
  if (window.scrollY >= document.documentElement.scrollHeight - 2 * window.innerHeight) {
    loadUsers();
    window.removeEventListener('scroll', checkPageScroll);
  }
}

// INITIAL - pradiniai veiksmai užkrovus puslapį pirmąjį kartą
root.appendChild(loadingAnimation);

// Ši funkcija įsivykdys tik kai bus parsiusti ir suformuoti duomenys
api.getUsers(initUserLoad);
window.addEventListener('scroll', checkPageScroll);

