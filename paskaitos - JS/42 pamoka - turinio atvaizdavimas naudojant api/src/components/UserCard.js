import defaultImg from '../assets/img/no-user-img.png';
import { formHtmlStructure } from '../utils/htmlContentHelpers';

export default class UserCard {
  static requiredProps = [
    'username',
    'description',
    'level',
    'level_curr_exp',
    'level_next_exp',
    'level_percent',
    'images',
    'url',
  ];

  static imageSizes = {
    thumb: 40,
    small: 150,
    medium: 350,
    large: 700,
  }

  static maxSymbols = 110;

  constructor(props) {
    this.element = document.createElement('article');
    this.element.className = 'user-card';
    UserCard.requiredProps.forEach(
      prop => this[prop] = props[prop] || props[prop] === 0
        ? props[prop]
        : ''
    );
  }

  trimDescription = (description) => {
    if (description) {
      const {
        slicedContent,
        htmlStructure,
        contentSymbolCount
      } = formHtmlStructure(description, UserCard.maxSymbols);
      // TODO: reikia patrumpinti turinį pagal htmlStructure
      // Atkirpimo (salyginę) funkciją reikėtų aprašyti 'utils/htmContenthelpers.js' faile
      // console.log(slicedContent);
      // console.log(htmlStructure);
      // console.log(contentSymbolCount);
      return slicedContent;
    }
    return 'No description';
  }

  mount() {
    const img = typeof this.images.small === 'string' ? this.images.small : defaultImg;
    const desc = this.trimDescription(this.description);
    const isNewUser = this.level === 0;
    const levelProgress = !isNewUser
      ? `${this.level_curr_exp} | ${this.level_next_exp}`
      : 'No experience';
    this.element.innerHTML = `
      <img 
        class="user-card__img" src="${img}" 
        height="${UserCard.imageSizes.small}px" 
        width="${UserCard.imageSizes.small}px"
      />
      <h2 class="user-card__header">
        <a href="${this.url}" target="blank">${this.username}</a>
        <span class="user-card__level">${this.level}</span>
      </h2>
      <div class="user-card__level-progress">
        <span 
          class="user-card__level-progress__xp-bg"
          style="width: ${isNewUser ? 0 : this.level_percent + '%'}">
        </span>
        <span class="user-card__level-progress__text">${levelProgress}</span>
      </div>
      <div>
      ${desc}
      </div>
    `;
    return this.element;
  }
}