//https://www.boardgameatlas.com/api/docs/users
const getUsers = (callback, from) => {
  fetch(`https://www.boardgameatlas.com/api/users?client_id=JLBr5npPhV${from ? '&skip=' + from : ''}`)
    .then(response => response.json())
    .then(data => callback(data.users))
    .catch(error => console.error(
      `Klaida:\n\tapi/index.js, getUsers(...).\nNepavyko gauti teigiamo atsakymo:\n\t${error}`
    ));
}

export default {
  getUsers
}