const tbody = document.querySelector('#js-table>tbody');

function createRow({ title, author, year, genre }) {
  const tr = document.createElement('tr');
  tr.className = 'table__body__row';
  tr.innerHTML = `
    <td>${title}</td>
    <td>${author}</td>
    <td>${year}</td>
    <td>${genre}</td>
  `;
  tbody.appendChild(tr);
}

books.forEach(createRow);