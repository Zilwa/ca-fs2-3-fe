class Validator {
  // Factory funkcija tai funkcija kuri formuoja funkcijas/objektus,
  // priklausomai nuo perduotų parametrų
  static createNumberInRangeValidator = (min, max) => {
    return {
      test: value => value >= min && value <= max,
      errorMsg: `Number must be from ${min} to ${max}`
    }
  }

  static isStringValidator = {
    test: value => typeof value === "string",
    errorMsg: "Input must be string"
  }

  static isNumberValidator = {
    test: value => !isNaN(value),
    errorMsg: "Input must be a number"
  }


}

