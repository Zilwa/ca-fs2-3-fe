class CrudManager {
  constructor(collection, selector) {
    this.collection = collection;
    this.container = document.querySelector(selector);
    this.headersSettings = this.createHeadersSettings();
    this.addForm = new Form(
      this.headersSettings.map(this.createFormInputSettings),
      this.addElement
    );
  }

  createFormInputSettings = settings => {
    const validators = [];
    let type;
    switch (settings.dataType) {
      case 'string':
        validators.push(Validator.isStringValidator);
        type = 'text';
        break;
      case 'number':
        validators.push(Validator.isNumberValidator);
        type = 'number';
        break;
      case 'boolean':
        type = 'radio';
        break;
      default:
        type = 'text';
    }
    return {
      name: settings.propName,
      value: '',
      type: type,
      validators: validators
    }
  }

  createHeadersSettings = () => {
    const headersSettings = [];
    for (const prop in this.collection[0]) {
      headersSettings.push({
        propName: prop,
        headerName: prop.charAt(0).toUpperCase() + prop.slice(1),
        dataType: typeof this.collection[0][prop]
      })
    }
    headersSettings.forEach(headerSettings => {
      for (let i = 1; i < this.collection.length; i++) {
        const el = this.collection[i];
        if (typeof el[headerSettings.propName] !== headerSettings.dataType) {
          headerSettings.dataType = 'mixed';
          break;
        }
      }
    })
    return headersSettings;
  }

  addElement = (formData) => {
    const element = formData.reduce((ob, inputOb) => {
      ob[inputOb.name] = inputOb.value;
      return ob;
    }, {});
    this.collection.unshift(element);
    this.render();
  }

  createTableHeaders = () => {
    return this.headersSettings.map((header) => {
      const tableHeaderElement = document.createElement('th');
      tableHeaderElement.className = 'sort-header';
      tableHeaderElement.innerHTML = `<span>${header.headerName}</span>`;
      if (header.dataType != 'mixed') {
        tableHeaderElement.innerHTML += '<span class="js-sort-header__arrow sort-arrow"></span>';
        // Cia bus uzdedami event listeneriai skirti rikiavimui
      }
      return tableHeaderElement;
    });
  }

  createDataRows = () => {
    return this.collection.map(el => {
      const tr = document.createElement('tr');
      for (const prop in el) {
        const td = document.createElement('td');
        td.innerHTML = el[prop];
        tr.appendChild(td);
      }
      return tr;
    });
  }

  render = () => {
    this.container.innerHTML = null;
    // Į root element įdedamas formos metodo render rezultatas;
    this.container.appendChild(this.addForm.render());
    // Formuojami lentelei reikalingi duomenys
    const tableHeaders = this.createTableHeaders();
    const dataRows = this.createDataRows();
    // Kuriami lentelės atvaizdavimui reikalingi elementai
    const table = document.createElement('table');
    table.className = 'table table-striped';
    table.innerHTML = `
    <thead class="thead thead-dark">
      <tr></tr>
    </thead>
    <tbody class="js-table-data"></tbody>`;
    // Surandama eilutė, į kurią bus dedamos duformuotos antraštės
    const headerRow = table.querySelector('tr');
    // Į antraštės eilutę dedamos antraštės
    tableHeaders.forEach(th => headerRow.appendChild(th));

    // Surandamas elementas <tbody> į kurį bus dedamos duomenų eilutės
    const tbody = table.querySelector('.js-table-data');
    // į elementą <tbody> dedamos eilutės
    dataRows.forEach(tr => tbody.appendChild(tr));
    // Lentelė įdedama į root element
    this.container.appendChild(table);
  }
}