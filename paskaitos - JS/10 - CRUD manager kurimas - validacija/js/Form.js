/* masyva sudaryta is objektu, pvz. :
  setting structure: [
    {
      name: 'brand',
      type: 'number'
      value: 'BMW',
      validators: [
        {
          validate: function1, - turi gražinti true, jeigu validacija sėkminga
          errorMsg: functionX1
        },
        ...
      ]
    }, 
    ...
    {
      name: [string],
      value:  [string],
      type: 'number'
      validators: [
        {
          validate: function1,
          errorMsg: functionX1
        },
        ...
      ]
    }, 
  ]
*/
class Form {
  constructor(settings, onSuccess) {
    this.settings = settings;
    this.onSuccess = onSuccess;
  }

  onSubmit = (e) => {
    e.preventDefault();
    const data = Array
      .from(e.target) // e target - siuo atveju visa forma
      .filter(input => input.name)
      .reduce((resultArr, input, i, arr) => {
        let value;
        if (arr.findIndex(({ name }) => name === input.name) === i) {
          switch (input.type) {
            case 'radio':
              value = e.target.querySelector(`input[name="${input.name}"]:checked`).value === 'true';
              break;
            case 'text':
              value = input.value;
              break;
          }
          return [
            ...resultArr,
            {
              name: input.name,
              value
            }
          ];
        }
        return resultArr;
      }, [])
    if (this.validateInputs(data)) {
      this.onSuccess(data)
    } else {
      console.log('nepavyko validacija');
      // Error spausdinimo logika
    }
  }

  validateInputs = (formData) => {
    const errors = [];
    formData.forEach(input => {
      const { validators } = this.settings.find(({ name }) => name === input.name);
      validators.forEach(({test, errorMsg}) => {
        if(!test(input.value)) errors.push({
          name: input.name,
          errors: [errorMsg]
        })
      });
    });
    console.log(errors);
    // Validavimas
    return errors.length === 0;
  }

  createInputs = () => {
    return this.settings.map(({ name, type, value }) => {
      const div = document.createElement('div');
      div.className = "col-12 col-sm-6 col-md-4 col-lg-3 mb-3";
      const label = document.createElement('label');
      label.innerHTML = name.charAt(0).toUpperCase() + name.slice(1);
      div.appendChild(label);
      switch (type) {
        case 'radio':
          const radioContainer = document.createElement('div');
          radioContainer.className = "d-flex";
          radioContainer.innerHTML = `
            <div class="mr-3">
              <label for="${name}-yes">Yes</label> 
              <input type="radio" name="${name}" value="true" id="${name}-yes">
            </div>
            <div>
              <label for="${name}-no">No</label> 
              <input type="radio" name="${name}" value="false" id="${name}-no" checked>
            </div>`;
          div.appendChild(radioContainer);
          break;
        default:
          label.setAttribute('for', name);
          const input = document.createElement('input');
          input.className = 'form-control';
          input.setAttribute('type', 'text');
          input.setAttribute('id', name);
          input.setAttribute('name', name);
          input.setAttribute('required', '');
          input.setAttribute('value', value);
          div.appendChild(input);
      }
      // Kaitos/perdirbimo funkcijos grąžinimas
      return div;
    });
  }

  createSubmitButton = () => {
    const btn = document.createElement('button');
    btn.className = "btn btn-success d-block w-25 mx-auto my-3";
    btn.innerHTML = 'Submit';
    return btn;
  }

  render = () => {
    const form = document.createElement('form');
    form.className = 'form border border-success p-3 rounded mb-4';
    form.innerHTML = `
      <div class="h2 text-center mb-3">Formos pavadinimas</div>
      <div class="row"></div>`;
    // Input'ų kūrimas ir pridėjimas į eilutę
    const row = form.querySelector('.row');
    const inputs = this.createInputs();
    inputs.forEach(input => row.appendChild(input));
    // Mygtuko kūrimas ir pridėjimas į formą
    const btn = this.createSubmitButton();
    form.appendChild(btn);
    // Uždedamas klausiklis submit'inant form'ą
    form.addEventListener('submit', this.onSubmit);
    return form;
  }
}