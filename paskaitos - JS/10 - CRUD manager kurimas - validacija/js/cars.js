const cars = [
  {
    brand: "Opel", // string 
    model: "Astra", // string
    year: 2005, // number
    color: "blue", // string
    price: 5000, // number
    damaged: false // boolean
  },
  {
    brand: "Opel",
    model: "Astra",
    year: 2007,
    color: 'blue',
    price: 1400,
    damaged: false
  },
  {
    brand: "BMW",
    model: "X5",
    year: 2012,
    color: 'black',
    price: 14000,
    damaged: false
  },
  {
    brand: "BMW",
    model: "X4",
    year: 2012,
    color: 'black',
    price: 10000,
    damaged: true
  },
  {
    brand: "BMW",
    model: "X4",
    year: 2012,
    color: 'black',
    price: 16000,
    damaged: false
  },
  {
    brand: "BMW",
    model: "X4",
    year: 2012,
    color: 'black',
    price: 9000,
    damaged: true
  },
  {
    brand: "BMW",
    model: "X4",
    year: 2012,
    color: 'black',
    price: 19000,
    damaged: false
  },
  {
    brand: "BMW",
    model: "X3",
    year: 2011,
    color: 'black',
    price: 11000,
    damaged: true
  },
  {
    brand: "Toyota",
    model: "Yaris",
    year: 2002,
    color: 'yellow',
    price: 1100,
    damaged: false
  },
  {
    brand: "Toyota",
    model: "Avensis",
    year: 2014,
    color: 'blue',
    price: 10000,
    damaged: true
  },
];