// https://edabit.com/challenge/ZngT4zDckDugt2JGY
{
  console.groupCollapsed('Class properties and methods');
  class Player {
    constructor(name, age = 'Unknown', height, weight) {
      // Validacija
      this.name = name;
      this.age = age;
      this.height = height;
      this.weight = weight;
    }

    getAge() {
      return `${this.name} is age ${this.age}`;
    }

    getHeight() {
      return `${this.name} is ${this.height}cm`;
    }

    getWeight() {
      return `${this.name} weighs ${this.weight}kg`;
    }
  }
  // new - išskiriama nuorodos tipo atminties vieta, į ją įrašome Player prototipo
  // objektą, perduodant parametrus Person klasės konstruktoriui
  const p1 = new Player("David Jones", 25, 175, 75);
  console.log(p1.getAge()); // ➞ "David Jones is age 25"
  console.log(p1.getHeight()); // ➞ "David Jones is 175cm"
  console.log(p1.getWeight()); // ➞ "David Jones weighs 75kg"
  console.groupEnd();
}

// https://edabit.com/challenge/kGLhgwGaLJsCMS7wS
{
  console.groupCollapsed('Class property formatting');
  class Employee {
    constructor(firstname, lastname) {
      this.firstname = firstname;
      this.lastname = lastname;
      this.fullname = firstname + ' ' + lastname;
      this.fullname = `${firstname} ${lastname}`;
      this.email = `${firstname}.${lastname}@company.com`.toLowerCase();
    }
  }

  const emp1 = new Employee("John", "Smith");
  console.log(emp1.fullname); // ➞ "John Smith"

  const emp2 = new Employee("Mary", "Sue");
  console.log(emp2.email); // ➞ "mary.sue@company.com"

  const emp3 = new Employee("Antony", "Walker");
  console.log(emp3.firstname); // ➞ "Antony"
  console.groupEnd();
}

// https://edabit.com/challenge/iwdZiFucR5wkQsFHu
{
  console.groupCollapsed('Class instances comparison');
  class Person {
    constructor(name, age) {
      this.name = name;
      this.age = age;
    }

    compareAge(other) {
      /*
      | method call       | this | other | this.name | this.age | other.name | other.age | return                      |
      | -------------------------------------------------------------------------------------------------------------- |
      | p1.compareAge(p2) | p1   | p2    | Samuel    |       24 | Joel       |        36 | Joel is older than me.      |
      | p2.compareAge(p1) | p2   | p1    | Joel      |       36 | Samuel     |        24 | Samuel is younger than me.  |
      | p1.compareAge(p3) | p1   | p3    | Samuel    |       24 | Lily       |        24 | Lily is the same age as me. |
      */
      if (other.age > this.age) return `${other.name} is older than me.`;
      else if (this.age === other.age) return `${other.name} is the same age as me.`;
      return `${other.name} is younger than me.`;
    }
  }

  const p1 = new Person("Samuel", 24);
  const p2 = new Person("Joel", 36);
  const p3 = new Person("Lily", 24);

  console.log(p1.compareAge(p2)); // ➞ "Joel is older than me."
  console.log(p2.compareAge(p1)); // ➞ "Samuel is younger than me."
  console.log(p1.compareAge(p3)); // ➞ "Lily is the same age as me."
  console.groupEnd();
}

// https://edabit.com/challenge/yxKoCKemzacK6PECM
{
  console.groupCollapsed('Class methods');
  class Calculator {
    // Write functions to add(), subtract(), multiply() and divide()
    add(a, b) {
      return a + b;
    }
    subtract(a, b) {
      return a - b;
    }
    multiply(a, b) {
      return a * b;
    }
    divide(a, b) {
      return a / b;
    }
  }

  const calculator = new Calculator();
  console.log(calculator.add(10, 5)); // ➞ 15
  console.log(calculator.subtract(10, 5)); // ➞ 5
  console.log(calculator.multiply(10, 5)); // ➞ 50
  console.log(calculator.divide(10, 5)); // ➞ 2
  console.groupEnd();
}

// https://edabit.com/challenge/Hgb38yhWGwJCMHbRQ
{
  console.groupCollapsed('Class properties and methods');
  class Circle {
    constructor(r) {
      this.r = r;
    }
    getArea() {
      return Math.PI * this.r ** 2;
    }
    getPerimeter() {
      return 2 * Math.PI * this.r;
    }
  }

  let q = new Circle(4.44);
  console.log(q.getArea());
  console.log(q.getPerimeter());
  console.groupEnd();
}

// https://edabit.com/challenge/qNMtrtizgssAQqP2b
{

  console.groupCollapsed('Class property formatting, slice([<start> [, <count>]]), charAt(<index>), <string>[<index>]');
  class Name {
    constructor(name, surname) {
      this.fname = name.slice(0, 1).toUpperCase() + name.slice(1).toLowerCase();
      this.lname = surname.charAt(0).toUpperCase() + surname.slice(1).toLowerCase();
      this.fullname = this.fname + ' ' + this.lname;
      this.initials = this.fname[0] + '.' + this.lname.slice(0, 1);
    }
  }

  const a1 = new Name("jOhN", "SMiTH");
  console.log(a1.fname); // ➞ "John"
  console.log(a1.lname); // ➞ "Smith"
  console.log(a1.fullname); // ➞ "John Smith"
  console.log(a1.initials); // ➞ "J.S"
  console.groupEnd();
}

// https://edabit.com/challenge/HKmJFmZZCX53ff4ke
{
  console.groupCollapsed('Using class instances as parameters');
  class IceCream {
    static flavors = {
      'Plain': 0,
      'Vanilla': 5,
      'ChocolateChip': 5,
      'Strawberry': 10,
      'Chocolate': 10,
    }

    constructor(flavor, sprCount) {
      // this.sweetness = IceCream.flavors[flavor] + sprCount;
      this.flavor = flavor;
      this.sprCount = sprCount;
    }

    getSweetness() {
      return IceCream.flavors[this.flavor] + this.sprCount;
    }

  }

  function sweetestIcecreamLocal(arr) {
    // 1. array.map + Math.max
    return Math.max(...arr.map(ice => ice.getSweetness()));
    // 2. array.reduce
    return arr.reduce((max, ice) => Math.max(max, ice.getSweetness()), arr[0].getSweetness());
  }

  const ice1 = new IceCream("Chocolate", 13);         // value of 23
  const ice2 = new IceCream("Vanilla", 0);           // value of 5
  const ice3 = new IceCream("Strawberry", 7);        // value of 17
  const ice4 = new IceCream("Plain", 18);             // value of 18
  const ice5 = new IceCream("ChocolateChip", 3);      // value of 8


  console.log(sweetestIcecreamLocal([ice1, ice2, ice3, ice4, ice5])); // ➞ 23
  console.log(sweetestIcecreamLocal([ice3, ice1])); // ➞ 23
  console.log(sweetestIcecreamLocal([ice3, ice5])); // ➞ 17

  // Atsakymas užduočiai
  const flavors = {
    'Plain': 0,
    'Vanilla': 5,
    'ChocolateChip': 5,
    'Strawberry': 10,
    'Chocolate': 10,
  };

  function countSweetness(ice) {
    return flavors[ice.flavor] + ice.numSprinkles;
  }

  function sweetestIcecream(arr) {
    // 1. array.map + Math.max
    return Math.max(...arr.map(countSweetness));
    // 2. array.reduce
    return arr.reduce((max, ice) => Math.max(max, countSweetness(ice)), countSweetness(arr[0]));
  }

  // ** Super n.d. nebūtina, bet jei padarysit būsit alfaaaaa:
  // https://medium.com/tech-tajawal/javascript-classes-under-the-hood-6b26d2667677
  //  pridėti prie klasės saldumo skaičiavimo metodą, ne klasės viduje, o naudojant:
  //  <objektas>.__proto__.getSweetness = function () { ... jūsų logika ...}
  //  Jeigu reikia, naudokite funkcinį prototipų aprašymą.
  //  Sėkmės.
  console.groupEnd();
}

