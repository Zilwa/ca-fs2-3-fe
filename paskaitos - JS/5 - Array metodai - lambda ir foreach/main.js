class ShopItem {
  constructor(name, price, amount, category) {
    this.name = name;
    this.price = price;
    this.amount = amount;
    this.category = category;
  }

  allItemsPrice() {
    return this.price * this.amount;
  }

  printNameAndCategory() {
    console.log(this.name + ' - ' + this.category);
  }

  formRowString(){
    return `
    <tr>
      <td>${this.name}</td>
      <td>${this.price} €</td>
      <td>${this.amount}</td>
      <td>${this.category}</td>
    </tr>`;
  }

  formRowElement(){
    const row = document.createElement('tr');
    // const nameTd = document.createElement('td');
    // nameTd.innerHTML = this.name;
    // row.appendChild(nameTd);

    // const priceTd = document.createElement('td');
    // priceTd.innerHTML = this.price;
    // row.appendChild(priceTd);

    // const amountTd = document.createElement('td');
    // amountTd.innerHTML = this.amount;
    // row.appendChild(amountTd);

    // const categoryTd = document.createElement('td');
    // categoryTd.innerHTML = this.category;
    // row.appendChild(categoryTd);

    [this.name, this.price, this.amount, this.category].forEach(tdName => {
      const td = document.createElement('td');
      td.innerHTML = tdName;
      row.appendChild(td);
      // Cia galėtume pridėt event listenerius kiekvienam td elementui
    });
    
    return row;
  }
}

const shopItems = [
  new ShopItem('Megztukas', 15, 12, 'Rūbai'), 
  new ShopItem('Komoda', 150, 7, 'Baldai'),
  new ShopItem('Suknelė', 17, 9, 'Rūbai'),
  new ShopItem('Siurblys', 229, 4, 'Tech. įranga'),
  new ShopItem('Virdulys', 39, 36, 'Tech. įranga'),
  new ShopItem('Vyr. Batai', 89, 6, 'Rūbai'),
  new ShopItem('Stalas', 320, 7, 'Baldai')
];

console.table(shopItems);
// 1. Parašyti metodą ShopItem prototipui, jog jis suskaičiuotų visų 'ShopItem' prekių vertę.
// Visų prekių sumines kainas atspausdinti konsolėje. 
// Spausdinimui naudoti 'arr.foreach' iteracinį metodą
shopItems.forEach(shopItem => console.log(shopItem.name + ' - ' + shopItem.allItemsPrice()));
// shopItems.forEach(function (shopItem) {
//   console.log(shopItem.name + ' - ' + shopItem.allItemsPrice());
// });


// 2. Parašyti metodą ShopItem prototipui, jog jis atspausdintų prekės pavadinimą ir kategoriją.
// Spausdinimui naudoti 'arr.foreach' iteracinį metodą
shopItems.forEach(shopItem => shopItem.printNameAndCategory());

// 3. Parašyti metodą ShopItem prototipui, kuris suformuotu HTML eilutę (string pavidalu) iš savybių:
//    name, price, amount, category;
// pvz:
// <tr>
//  <td>Pienas</td>
//  <td>0.89</td>
//  <td>13</td>
//  <td>Food</td>
// </tr>
// Spausdinimui naudoti 'arr.foreach' iteracinį metodą
shopItems.forEach(shopItem => console.log(shopItem.formRowString()));


// 4. Parašyti metodą ShopItem prototipui, kuris suformuotu HTML eilutę (HTMLElement pavidalu) iš savybių:
//    name, price, amount, category;
// tiek 'tr', tiek 'td' kurti kaip HTMLElement
// P.S.:
//    document.createElement(... elemento tag name string pavidalu ...)
//    target.appendChild(... i gala prijungimas vaikinis elementas ...)
//    target.innerHTML = ... nustatoma reikšmė 'target' elemento viduje ...
// Spausdinimui naudoti 'arr.foreach' iteracinį metodą
shopItems.forEach(shopItem => console.log(shopItem.formRowElement()));
