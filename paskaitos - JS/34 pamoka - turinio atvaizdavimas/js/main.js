const paymentTable = document.querySelector('#payment-table');
const paymentTableBody = paymentTable.querySelector('tbody');

const columns = [
  {
    title: 'Pilnas vardas',
    callback: ({ fullname }) => fullname || '—'
  },
  {
    title: 'Specialybė',
    callback: ({ profession }) => profession || '—'
  },
  {
    title: 'Įdarbinimo pobūdis',
    callback: ({ contract }) => contract === 'ds'
      ? 'Darbo sutartis'
      : contract === 'dsn'
        ? 'Darbo santykiai'
        : 'Individuali veikla'
  },
  {
    title: 'Mėnesinis atlyginimas',
    callback: ({ salary }) => salary ? salary + " €" : '—'
  },
  {
    title: 'Valandinis įkainis',
    callback: ({ hourPay }) => hourPay ? hourPay + " €" : '—'
  },
  {
    title: 'Valandų skaičius',
    callback: ({ hourCount }) => hourCount || '—'
  },
  {
    title: 'Užmokestis',
    callback: ({ pay }) => pay ? pay + " €" : '—'
  },
  {
    title: 'Viso',
    callback: person => (person.contract === 'ds'
      ? person.salary 
      : person.contract === 'dsn'
        ? person.pay
        : person.hourCount * person.hourPay)  + " €"
  },
];

people.forEach(person => {
  const row = document.createElement('tr');
  columns.forEach(column => {
    const td = document.createElement('td');
    td.innerHTML = column.callback(person);
    row.appendChild(td);
  });
  paymentTableBody.appendChild(row)
});

