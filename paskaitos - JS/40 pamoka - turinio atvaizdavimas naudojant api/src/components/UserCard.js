export default class UserCard {
  constructor(props) {
    console.log(props);
    const {username} = props;
    this.element = document.createElement('article');
    this.element.className = 'user-card';
    this.username = username
  }

  mount() {
    this.element.innerHTML = `<h2>${this.username}</h2>`;
    return this.element;
  }
}