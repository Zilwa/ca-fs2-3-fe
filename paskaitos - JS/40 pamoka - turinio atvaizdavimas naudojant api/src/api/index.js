//https://www.boardgameatlas.com/api/docs/users
const getUsers = (callback) => {
  fetch('https://www.boardgameatlas.com/api/users?client_id=JLBr5npPhV')
    // .then(response => response.json())
    // .then(data => callback(data.users))
    // .catch(error => console.error(
    //   `Klaida:\n\tapi/index.js, getUsers(...).\nNepavyko gauti teigiamo atsakymo:\n\t${error}`
    // ));
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      callback(data.users)
    })
    .catch(function (error) {
      console.error(`Klaida:\n\tapi/index.js, getUsers(...).\nNepavyko gauti teigiamo atsakymo:\n\t${error}`)
    });
}

export default {
  getUsers
}