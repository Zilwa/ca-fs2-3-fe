import api from './api';
import UserCard from './components/UserCard';
import './scss/main.scss';

const root = document.querySelector('#app');
let userCards;

const mountUsers = () => {
  root.innerHTML = '';
  userCards.forEach(userCard => root.appendChild(userCard.mount()));
}

api.getUsers((users) => {
  // Ši funkcija įsivykdys tik kai bus parsiusti ir suformuoti duomenys
  userCards = users.map(userData => new UserCard(userData));
  mountUsers();
});

