class Validator {
  // Factory funkcija tai funkcija kuri formuoja funkcijas/objektus,
  // priklausomai nuo perduotų parametrų
  static createIsNumberValidator = (min, max) => {
    return {
      validator: (value) => {
        return value >= min && value <= max
      },
      errorMsg: `Number must be from ${min} to ${max}`
    }
  }
}