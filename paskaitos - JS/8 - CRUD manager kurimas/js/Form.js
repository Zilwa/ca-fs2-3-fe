/* masyva sudaryta is objektu, pvz. :
  setting structure: [
    {
      name: 'brand',
      value: 'BMW',
      validators: [
        {
          validator: function1,
          errorMsg: functionX1
        },
        ...
      ]
    }, 
    ...
    {
      name: [string],
      value:  [string],
      validators: [
        {
          validator: function1,
          errorMsg: functionX1
        },
        ...
      ]
    }, 
  ]
*/
class Form {
  constructor(settings, onSuccess) {
    this.settings = settings;
    this.onSuccess = onSuccess;
  }

  onSubmit = (e) => {
    e.preventDefault();
    const data = Array
      .from(e.target)
      .filter(input => !input.classList.contains('btn'))
      .map(input => ({
        name: input.id,
        value: input.value
      }));
    if (this.validateInputs(data)) {
      // Kviečiama prototipui perduoda sekmės funkcija, jeigu pavyko validacija. 
      // Perduodami pravaliduoti duomenys 
      this.onSuccess(data)
    } else {
      // Error spausdinimo logika
    }
  }

  validateInputs = () => {
    // Validavimo logika ...
    return true;
  }

  createInputs = () => {
    return this.settings.map(inputSettings => {
      const div = document.createElement('div');
      div.className = "col-12 col-sm-6 col-md-4 col-lg-3 mb-3";
      // Sukuriamas ir suformuojamas įvesties lauko 'label'
      const label = document.createElement('label');
      label.innerHTML = inputSettings.headerName;
      label.setAttribute('for', inputSettings.propName);
      div.appendChild(label);
      // Sukuriamas įvesties laukas
      const input = document.createElement('input');
      input.className = 'form-control';
      input.setAttribute('type', 'text');
      input.setAttribute('id', inputSettings.propName);
      input.setAttribute('required', '');
      // !!!!!!!!!!!!!!!!!!!!! TIK TESTAVIMUI !!!!!!!!!!!!!!!!!!!!!!
      input.setAttribute('value', 'test');
      // !!!!!!!!!!!!!!!!!!!!! TIK TESTAVIMUI !!!!!!!!!!!!!!!!!!!!!!
      div.appendChild(input);
      // Kaitos/perdirbimo funkcijos grąžinimas
      return div;
    });
  }

  createSubmitButton = () => {
    const btn = document.createElement('button');
    btn.className = "btn btn-success d-block w-25 mx-auto my-3";
    btn.innerHTML = 'Submit';
    return btn;
  }

  render = () => {
    const form = document.createElement('form');
    form.className = 'form border border-success p-3 rounded mb-4';
    form.innerHTML = `
      <div class="h2 text-center mb-3">Formos pavadinimas</div>
      <div class="row"></div>`;
    // Input'ų kūrimas ir pridėjimas į eilutę
    const row = form.querySelector('.row');
    const inputs = this.createInputs();
    inputs.forEach(input => row.appendChild(input));
    // Mygtuko kūrimas ir pridėjimas į formą
    const btn = this.createSubmitButton();
    form.appendChild(btn);
    // Uždedamas klausiklis submit'inant form'ą
    form.addEventListener('submit', this.onSubmit);
    return form;
  }
}