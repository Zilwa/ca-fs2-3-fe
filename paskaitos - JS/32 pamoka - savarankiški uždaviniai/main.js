// Užduotys: https://edabit.com/challenge/nuXdWHAoHv9y38sn7
// JavaScript: Easy -> Objects

// 1 - https://edabit.com/challenge/nuXdWHAoHv9y38sn7
{
  function sortDrinkByPrice(drinks) {
    return drinks.sort((a, b) => a.price - b.price);
  }
  console.groupCollapsed('1. Sort');
  const drinks = [
    { name: "lime", price: 10 },
    { name: "vodka", price: 30 },
    { name: "lemonade", price: 50 },
    { name: "tequila", price: 70 }
  ];
  const sortedDrinks = sortDrinkByPrice([...drinks]);
  console.table(drinks);
  console.table(sortedDrinks);
  console.groupEnd();
}

// 2 - https://edabit.com/challenge/9KEKJG5PZTFmG3Zau
{
  function addName(obj, name, value) {
    obj[name] = value;
    return obj;
    return {
      ...obj,
      [name]: value
    }
    const copy = JSON.parse(JSON.stringify(obj));
    copy[name] = value;
    return copy;
  }
  console.groupCollapsed('2. Add property to object');
  console.log('addName({}, "Brutus", 300) ⇨ ',
    addName({}, "Brutus", 300)); // ➞ { Brutus: 300 }
  console.log('addName({ piano: 500 }, "Brutus", 400) ⇨ ',
    addName({ piano: 500 }, "Brutus", 400)); // ➞ { piano: 500, Brutus: 400 }
  console.log('addName({ piano: 500, stereo: 300 }, "Caligula", 440) ⇨ ',
    addName({ piano: 500, stereo: 300 }, "Caligula", 440)); // ➞ { piano: 500, stereo: 300, Caligula: 440 }
  console.groupEnd();
}

// 3 - https://edabit.com/challenge/48EJWLhF224na8po3
{
  const generations = {
    '-3': { m: 'great grandfather', f: 'great grandmother' },
    '-2': { m: 'grandfather', f: 'grandmother' },
    '-1': { m: 'father', f: 'mother' },
    '0': { m: 'me!', f: 'me!' },
    '1': { m: 'son', f: 'daughter' },
    '2': { m: 'grandson', f: 'granddaughter' },
    '3': { m: 'great grandson', f: 'great granddaughter' },
  };
  function generation(x, y) {
    // return generations[x][y];
    return [-3, -2, -1, 0, 1, 2, 3].includes(x) && ['m', 'f'].includes(y)
      ? ([, , 'grand', 'great grand'][Math.abs(x)] || '') +
      [(y === 'm' ? 'fa' : 'mo') + 'ther', 'me!', y === 'm' ? 'son' : 'doughter'][Math.sign(x) + 1]
      : 'Klaidingas formatas';
    if (x === 0) return 'me!';
    let str = '';
    switch (Math.abs(x)) {
      case 3: str += 'great ';
      case 2: str += 'grand';
    }
    if (y === 'm') {
      if (x <= -1) str += 'father';
      else str += 'son'
    } else {
      if (x <= -1) str += 'mother';
      else str += 'doughter'
    }
    return str;
  }
  console.groupCollapsed('3. Nested objects');
  console.log('generation(-3, "f")', generation(-3, "f"))
  console.log('generation(-2, "f")', generation(-2, "f"))
  console.log('generation(-1, "f")', generation(-1, "f"))
  console.log('generation(0, "f")', generation(0, "f"))
  console.log('generation(1, "f")', generation(1, "f"))
  console.log('generation(2, "f")', generation(2, "f"))
  console.log('generation(3, "f")', generation(3, "f"))
  console.log('generation(-3, "m")', generation(-3, "m"))
  console.log('generation(-2, "m")', generation(-2, "m"))
  console.log('generation(-1, "m")', generation(-1, "m"))
  console.log('generation(0, "m")', generation(0, "m"))
  console.log('generation(1, "m")', generation(1, "m"))
  console.log('generation(2, "m")', generation(2, "m"))
  console.log('generation(3, "m")', generation(3, "m"))
  console.groupEnd();

}


// 4 - https://edabit.com/challenge/i6YqzHcSiPiEQKjeX
{
  function maximumScore(tileHand) {

  }
  console.groupCollapsed('4. Array.prototype.reduce');
  console.log(
    `maximumScore([
    { tile: "N", score: 1 },
    { tile: "K", score: 5 },
    { tile: "Z", score: 10 },
    { tile: "X", score: 8 },
    { tile: "D", score: 2 },
    { tile: "A", score: 1 },
    { tile: "E", score: 1 }
  ]) ➞ `,
    maximumScore([
      { tile: "N", score: 1 },
      { tile: "K", score: 5 },
      { tile: "Z", score: 10 },
      { tile: "X", score: 8 },
      { tile: "D", score: 2 },
      { tile: "A", score: 1 },
      { tile: "E", score: 1 }
    ])
  ); // ➞ 28
  console.log(
    `maximumScore([
    { tile: "B", score: 2 },
    { tile: "V", score: 4 },
    { tile: "F", score: 4 },
    { tile: "U", score: 1 },
    { tile: "D", score: 2 },
    { tile: "O", score: 1 },
    { tile: "U", score: 1 }
  ]) ➞ `,
    maximumScore([
      { tile: "B", score: 2 },
      { tile: "V", score: 4 },
      { tile: "F", score: 4 },
      { tile: "U", score: 1 },
      { tile: "D", score: 2 },
      { tile: "O", score: 1 },
      { tile: "U", score: 1 }
    ])
  ); // ➞ 15
  console.groupEnd();
}

// 5 - https://edabit.com/challenge/8s2jy9hR2TAeQinKD
{
  function calculateDifference(obj, limit) {

  }
  console.groupCollapsed('5. Array.prototype.reduce + subtraction');
  console.log(
    'calculateDifference({ "baseball bat": 20 }, 5) →',
    calculateDifference({ "baseball bat": 20 }, 5)
  ); // ➞ 15
  console.log(
    'calculateDifference({ skate: 10, painting: 20 }, 19) →',
    calculateDifference({ skate: 10, painting: 20 }, 19)
  ); // ➞ 11
  console.log(
    'calculateDifference({ skate: 200, painting: 200, shoes: 1 }, 400) →',
    calculateDifference({ skate: 200, painting: 200, shoes: 1 }, 400)
  ); // ➞ 1
  console.groupEnd();
}

// 6 - https://edabit.com/challenge/pPNAs5PvB3WvnDwDM
{
  function toArray(obj) {

  }
  console.groupCollapsed('6. Object to array');
  console.log('toArray({ a: 1, b: 2 }) ➞', toArray({ a: 1, b: 2 })); // ➞ [["a", 1], ["b", 2]]
  console.log('toArray({ shrimp: 15, tots: 12 }) ➞', toArray({ shrimp: 15, tots: 12 })); // ➞ [["shrimp", 15], ["tots", 12]]
  console.log('toArray({}) ➞', toArray({})); // ➞ []
  console.groupEnd();
}

// 7 - https://edabit.com/challenge/AP4hnF97anRc2mAZ6
{
  function keysAndValues(obj) {

  }

  console.groupCollapsed('7. Custom formating');
  console.log('keysAndValues({ b: 1, c: 2, a: 3 }) →', keysAndValues({ b: 1, c: 2, a: 3 }));
  // ➞ [["a", "b", "c"], [3, 1, 2]]
  console.log('keysAndValues({ a: "Apple", b: "Microsoft", c: "Google" }) →', keysAndValues({ a: "Apple", b: "Microsoft", c: "Google" }));
  // ➞ [["a", "b", "c"], ["Apple", "Microsoft", "Google"]]
  console.log('keysAndValues({ key1: true, key2: false, key3: undefined }) →', keysAndValues({ key1: true, key2: false, key3: undefined }));
  // ➞ [["key1", "key2", "key3"], [true, false, undefined]]
  console.groupEnd();
}

// 8 - https://edabit.com/challenge/QXWM2oo7rQNiyDsip
{
  function inkLevels(inks) {

  }
  console.groupCollapsed('8. Math.min and Object.keys');
  console.log(
    `inkLevels({
    "cyan": 23,
    "magenta": 12,
    "yellow": 10
  }) ➞ 10 ➞`,
    inkLevels({
      "cyan": 23,
      "magenta": 12,
      "yellow": 10
    })
  );
  console.log(
    `inkLevels({
    "cyan": 432,
    "magenta": 543,
    "yellow": 777
  }) ➞ 432 ➞`,
    inkLevels({
      "cyan": 432,
      "magenta": 543,
      "yellow": 777
    })
  );
  console.log(
    `inkLevels({
    "cyan": 700,
    "magenta": 700,
    "yellow": 0
  }) ➞ 0 ➞`,
    inkLevels({
      "cyan": 700,
      "magenta": 700,
      "yellow": 0
    })
  );
  console.groupEnd();
}

// 9 - https://edabit.com/challenge/pLNavsePxJ87t9Nak
{
  function calculateLosses(obj) {
    return 0;
  }
  console.groupCollapsed('9. Array.prototype.reduce Object.values()');
  const results = [
    {
      tv: 30,
      skate: 20,
      stereo: 50,
    },
    {
      painting: 20000,
    },
    {}
  ].reduce((str, items) => {
    str += '{'
    str += Object.keys(items).reduce((str, prop) => str += `\n\t${prop}: ${items[prop]}`, '');
    str += str.length === 1 ? '}' : '\n}';
    str += ` ⇨ ${calculateLosses(items)}\n`;
    return str;
  }, '');

  console.log(results);
  console.groupEnd();
}

// 10 - Array.prototype.filter && Array.prototype.reduce
{
  function countTrue(arr) {

  }
  console.groupCollapsed('10 - Array.prototype.filter && Array.prototype.reduce');
  console.log('countTrue([true, false, false, true, false]) ➞ 2 ➞', countTrue([true, false, false, true, false])); // ➞ 2
  console.log('countTrue([false, false, false, false]) ➞ 0 ➞', countTrue([false, false, false, false])); // ➞ 0
  console.log('countTrue([]) ➞ 0 ➞', countTrue([])); // ➞ 0
  console.groupCollapsed('9. Array.prototype.reduce Object.values()');
}