// Įvedamas žodis naudojant promt metodą ir paverčiamas mažosiomis raidėmis korektiškam raidžių palyginimui
const word = prompt('Įveskite žodį:').toLocaleLowerCase();
// Nustatomas gyvybių kiekis
const lives = 7;
// Kintamasis, kuriame saugomos likusios gyvybės
let livesLeft = lives;
// Atspėtų raidžių kiekis
let lettersCorrect = 0;

// Iš HTML turinio pasirenkamas elementas, kuriame talpinsime pasleptas 'word' kintamojo raides
const wordContainer = document.querySelector('.word-container');
// Iš HTML turinio pasirenkamas elementas, kuriame talpinsime klaidų žymėjimui skirtus kvadračiukus
const mistakesContainer = document.querySelector('.mistakes-container');
// Formuojamas objektų masyvas žaidimo logikai
const keyboardLetterObjects = Array
  // Pasirenkami visi klaviatūros klavišai ir konvertuojami iš 'NodeList' į 'Array' prototipą
  .from(document.querySelectorAll('.keyboard__key'))
  // Formuojamas masyvas žaidimo logikai naudojant iteracinį 'array.prototype.map' metodą
  .map(keyEl => { // Su kiekvienu klaviatūros mygtuko elementu formuojamas ir grąžinamas objektas
    // Nuskaitoma mygtuko raidė
    const letter = keyEl.innerHTML;
    // keyEl.addEventListener('click', () => quessLetter(letter), { once: true });
    // Ant mygtuko uždedamas 'click' įvykio klausiklis, kuris vyks tik vieną kartą
    keyEl.addEventListener('click', handleLetterGuess, { once: true });
    // Mygtuko HTML objektui priskiriama reikšmė, jog vėliau būtų galima ją panaudoti
    keyEl.letter = letter;
    // Grąžinamas ir suformuojamas objektas, kuris bus išsaugotas 'keyboardLetterObjects' masyve
    return {
      letter, // Raidė
      correct: 'unknown', // Pažymėjimo statusas
      keyboardKey: keyEl // Mygtuko HTML elementas
    }
  });
// Masyvas kuriame bus saugomi spėjamo žodžio raidžių objektai žaidimo logikai
const wordLetterObjects = [];
// Masyvas kuriame bus saugomi klaidų žymėjimo kvadračiukai
const mistakeSquares = [];

// Pagal žodžio ilgį formuojamas atitinkamas nematomų raidžių masyvas 'wordLetterObjects'
for (let i = 0; i < word.length; i++) {
  // Nuskaitoma žodžio raidė ciklo darbinio kintamojo reikšmės indeksu
  const letter = word.charAt(i);
  // Sukuriamas html elementas: <span></span>
  const letterContainer = document.createElement('span');
  // Papildome klase: <span class="unknown-letter"></span>
  letterContainer.className = 'unknown-letter';
  // Įdedame turinį: <span class="unknown-letter">... Žodžio raidė ciklo darbinio kitamojo indeksu ....</span>
  letterContainer.innerHTML = letter;
  // Suformuojamas ir į masyvą 'wordLetterObjects' įdedamas spėjamajo žodžio objektas
  wordLetterObjects.push({ letter, letterContainer });
  // Žodžio raidės HTML elementas įdedamas į spėjamo žodžio konteinerį
  wordContainer.appendChild(letterContainer);
}

// Pagal gyvybių skaičių formuojamas 'mistakeSquares' elementų masyvas, klaidoms žymėti
for (let i = 0; i < lives; i++) {
  // Sukuriamas html elementas: <span></span>
  const mistakeSquare = document.createElement('span');
  // Papildome klase: <span class="mistake-square"></span>
  mistakeSquare.className = 'mistake-square';
  // Į masyvą 'mistakeSquares' įdedamas klaidai pymėti skirtas HTML elementas (kvadračiukas)
  mistakeSquares.push(mistakeSquare);
  // Kvadračiukas įdedamas į konteinerį skirtą klaidoms žymėti
  mistakesContainer.appendChild(mistakeSquare);
}

/**
 * Ši funkcija skirta iškviesti raidės spėjimą paspaudus mygtuką. 
 * Taip pat ji aprašome pavadinimu(deklaracija) tam, kad vėliau būtų galima ją nuimti
 * naudojant 'HTMLElement.removeEventListener(eventType, functionName)'
 * 
 * @param {object} target - iš įvykio ištraukiamas elementas, su kuriuo įvyko įvykis 
 */
function handleLetterGuess({ target }) {
  quessLetter(target.letter);
}

function quessLetter(letter) {
  const ii = keyboardLetterObjects.findIndex(keyOb => keyOb.letter === letter);
  if (word.includes(letter)) {
    keyboardLetterObjects[ii].correct = true;
    keyboardLetterObjects[ii].keyboardKey.classList.add('keyboard__key--good');
    wordLetterObjects
      .filter(wordLetterOb => wordLetterOb.letter === letter)
      .map(wordLetterOb => wordLetterOb.letterContainer)
      .forEach(wordLetterContainer => {
        lettersCorrect++;
        wordLetterContainer.classList.add('unknown-letter--answered')
      });
    if (lettersCorrect === word.length) winGame();
  } else {
    keyboardLetterObjects[ii].correct = false;
    keyboardLetterObjects[ii].keyboardKey.classList.add('keyboard__key--bad');
    mistakeSquares[lives - livesLeft--].classList.add('mistake-square--bad');
    if (livesLeft === 0) looseGame();
  }
}


function winGame() {
  alert('Laimejai');
  dissolveGame();
}

function looseGame() {
  alert('Pralaimejai');
  dissolveGame();
}

function dissolveGame() {
  keyboardLetterObjects.forEach(({ keyboardKey }) => keyboardKey.removeEventListener('click', handleLetterGuess))
}

quessLetter(word.charAt(0));