import api from './api';
import loadingGif from './assets/img/loading.gif';
import UserCard from './components/UserCard';
import './scss/main.scss';

const root = document.querySelector('#app');
let userCards;

root.innerHTML = `
<div class="loading-animation">
  <img src="${loadingGif}">
</div> 
`;

const mountUsers = () => {
  root.innerHTML = '';
  const cardContainer = document.createElement('div');
  cardContainer.className = 'card-container';
  root.appendChild(cardContainer);

  userCards.forEach(userCard => cardContainer.appendChild(userCard.mount()));
  setTimeout(() => cardContainer.classList.add('show'), 0);
}

api.getUsers((users) => {
  // Ši funkcija įsivykdys tik kai bus parsiusti ir suformuoti duomenys
  userCards = users.map(userData => new UserCard(userData));
  mountUsers();
});

