import defaultImg from '../assets/img/no-user-img.png';

export default class UserCard {
  static requiredProps = [
    'username',
    'description',
    'experience',
    'gold',
    'level',
    'level_curr_exp',
    'level_next_exp',
    'level_percent',
    'images',
    'url',
  ];

  static imageSizes = {
    thumb: 40,
    small: 150,
    medium: 350,
    large: 700,
  }

  constructor(props) {
    this.element = document.createElement('article');
    this.element.className = 'user-card';
    UserCard.requiredProps.forEach(prop => this[prop] = props[prop] || '');
  }

  mount() {
    const img = typeof this.images.small === 'string' ? this.images.small : defaultImg;
    this.element.innerHTML = `
      <img 
        class="user-card__img" src="${img}" 
        height="${UserCard.imageSizes.small}px" 
        width="${UserCard.imageSizes.small}px"
      />
      <h2 class="user-card__header">
        <a href="${this.url}" target="blank">${this.username}</a>
        <span class="user-card__level">${this.level}</span>
      </h2>
      <div class="user-card__level-progress">
        <span 
          class="user-card__level-progress__xp-bg"
          style="width: ${this.level_percent}%">
        </span>
        <span class="user-card__level-progress__text">
          ${this.level_curr_exp} | ${this.level_next_exp}
        </span>
      </div>
      ${this.description}
    `;
    return this.element;
  }
}