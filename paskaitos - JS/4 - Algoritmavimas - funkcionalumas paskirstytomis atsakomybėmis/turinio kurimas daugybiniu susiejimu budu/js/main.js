const tableData = document.querySelector('tbody.js-table-data');
const sortHeaders = document.querySelectorAll('.js-sort-header'); // ???

// FUNCTIONS -----------------------------------------------------------
/**
 * Ši funkcija atvaizduoja masyvo objektus eilutėmis tableData elemente
 */
function renderTableRows() {
  tableData.innerHTML = '';
  cars.forEach(function (car) {
    tableData.innerHTML += `
    <tr>
      <td>${car.brand}</td>
      <td>${car.model}</td>
      <td>${car.year}</td>
      <td>${car.price}</td>
      <td>${car.color}</td>
    </tr>`;
  });
}

/**
 * Ši funkcija vykdoma kuomet yra paspaudžiamas viena iš lentelių antraščių.
 * Jos tikslas pakeisti eilučių rikiavimą ir atnaujinti duomenis
 * 
 * @param {mouseEvent} event - informacija apie įvykusį įvykį 
 */
function handleSortOrder(event) {
  const arrowEl = assignArrowElement(event.target);
  const sortCriteria = assignSortCriteria(event.target);
  resetHeaderMarkers(sortCriteria);
  const order = changeArrowState(arrowEl.classList);
  sortItems(sortCriteria, order);
  renderTableRows();
}

/**
 * Ši funkcija rikiuoja masyvo elementus pagal tam tikrą kriterijų - objekto savybę.
 * @param {string} criteria - savybė objekte (object property)
 * @param {string} order - rikiavimo kryptis, DESC|ASC
 * 
 */
function sortItems(criteria, order) {
  const direction = order === 'DESC' ? -1 : 1;
  cars.sort(function (a, b) {
    switch (typeof a[criteria]) {
      case 'string':
        return a[criteria].localeCompare(b[criteria]) * direction;
      case 'number':
        return (a[criteria] - b[criteria]) * direction;
      default:
        console.error('Palyginimo funkcija nemoka atlikti šio tipo palyginimo');
        return 0;
    }
  });
}

/**
 * Keičia žymeklio klases, priklausomai nuo esamo klasių rinkinio
 * 
 * @param {DOMTokenList} classList - paspausto žymeklio CSS klasių rinkinys
 * 
 * @return {string} esam1 rikiavimo kryptį žodiniu formatu: DESC|ASC
 */
function changeArrowState(classList) {
  if (!classList.contains('sort-arrow--down')) {
    classList.add('sort-arrow--down');
    classList.remove('sort-arrow--up');
    return 'DESC';
  }
  classList.add('sort-arrow--up');
  classList.remove('sort-arrow--down');
  return 'ASC';
}

/**
 * Suranda žymeklio elementą antraštės HTML elemento ribose
 * 
 * @param {HTMLElement} target - įvykio metu paspaustas elementas
 * 
 * @return {HTMLElement} žymeklio elemntą
 */
function assignArrowElement(target) {
  if (target.classList.contains('js-sort-header__arrow')) {
    return target;
  } else if (target.classList.contains('js-sort-header')) {
    return target.lastElementChild;
  }
  return target.nextElementSibling;
}

/**
 * Suranda antraštėje esantį kriterijų - antraštės pavadinimą
 * @param {HTMLElement} target - įvykio metu paspaustas elementas
 * 
 * @returns {string} mažosiomis raidėmis paverstas antraštės pavadinimas 
 */
function assignSortCriteria(target) {
  if (target.classList.contains('js-sort-header__arrow')) {
    return target.previousElementSibling.innerHTML.toLowerCase();
  } else if (target.classList.contains('js-sort-header')) {
    return target.firstElementChild.innerHTML.toLowerCase();
  }
  return target.innerHTML.toLowerCase();
}

/**
 * Nustato visų lentelės antraščių klases į pradines, išskyrus paspaustosios
 * 
 * @param {string} selectedHeaderName - paspausto headerio pavadinimas 
 */
function resetHeaderMarkers(selectedHeaderName) {
  sortHeaders.forEach(function (header) {
    const headerName = header.querySelector('span').innerHTML.toLowerCase();
    if (headerName !== selectedHeaderName) {
      const marker = header.querySelector('.js-sort-header__arrow');
      marker.className = 'js-sort-header__arrow sort-arrow';
    }
  })
}

// INITIAL - PRADINĖS KOMANDOS -----------------------------------------------
renderTableRows();
sortHeaders.forEach(function (sortHeader) {
  sortHeader.addEventListener('click', handleSortOrder);
})
