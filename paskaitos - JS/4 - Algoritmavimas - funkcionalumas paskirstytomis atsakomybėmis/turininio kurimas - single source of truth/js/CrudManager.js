class CrudManager {
  constructor(collection, selector) {
    this.collection = collection;
    this.container = document.querySelector(selector);
  }

  createTableHeaders() {
    const headerNames = [];
    // Iteruojama per pirmojo masyvo objekto savybes (properties)
    for (const headerName in this.collection[0]) {
      // Kiekviena savybė string pavidalu dedama į headerNames masyvą
      headerNames.push(headerName)
    }
    const tableHeaders = headerNames.map(function (headerName) {
      const nameCapitalized = headerName.charAt(0).toUpperCase() + headerName.slice(1);
      const tableHeaderElement = document.createElement('th'); // <th></th>
      tableHeaderElement.className = 'sort-header'; // <th class="sort-header"></th>
      tableHeaderElement.innerHTML = `
        <span>${nameCapitalized}</span>
        <span class="js-sort-header__arrow sort-arrow"></span>`;
      // Cis bus uzdedami event listeneriai
      return tableHeaderElement;
    });
    return tableHeaders;
  }

  render() {
    const tableHeaders = this.createTableHeaders();
    this.container.innerHTML = `
    <table class="table table-striped">
      <thead class="thead thead-dark">
        <tr></tr>
      </thead>
      <tbody class="js-table-data"></tbody>
    </table>`;
    const headerRow = this.container.querySelector('tr');
    tableHeaders.forEach(function (tableHeader) {
      headerRow.appendChild(tableHeader);
    })
  }
}