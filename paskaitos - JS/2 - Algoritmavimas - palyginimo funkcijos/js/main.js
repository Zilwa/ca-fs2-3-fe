const tableData = document.querySelector('tbody.js-table-data');
const sortHeaders = document.querySelectorAll('.js-sort-header');

console.log(sortHeaders);

// functions
function render() {
  cars.forEach(function (car) {
    tableData.innerHTML += `
    <tr>
      <td>${car.brand}</td>
      <td>${car.model}</td>
      <td>${car.year}</td>
      <td>${car.price}</td>
      <td>${car.color}</td>
    </tr>`;
  });
}

function sortItems(event) {
  if (event.target.classList.contains('js-sort-header__arrow')) {
    console.log('paspaustas trikampiukas')
  } else if (event.target.classList.contains('js-sort-header')) {
    console.log('paspaustas headeris')
  } else {
    console.log('paspaustas pavadinimas')
  }

}


// Initial
render();
sortHeaders.forEach(function (sortHeader) {
  sortHeader.addEventListener('click', sortItems);
})


