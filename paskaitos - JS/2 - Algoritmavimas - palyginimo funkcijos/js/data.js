const cars = [
  {
    brand: "Opel",
    model: "Astra",
    year: 2005,
    color: 'blue',
    price: 1200
  },
  {
    brand: "Opel",
    model: "Astra",
    year: 2007,
    color: 'blue',
    price: 1400
  },
  {
    brand: "BMW",
    model: "X5",
    year: 2012,
    color: 'black',
    price: 14000
  },
  {
    brand: "BMW",
    model: "X4",
    year: 2012,
    color: 'black',
    price: 10000
  },
  {
    brand: "BMW",
    model: "X4",
    year: 2012,
    color: 'black',
    price: 16000
  },
  {
    brand: "BMW",
    model: "X4",
    year: 2012,
    color: 'black',
    price: 9000
  },
  {
    brand: "BMW",
    model: "X4",
    year: 2012,
    color: 'black',
    price: 19000
  },
  {
    brand: "BMW",
    model: "X3",
    year: 2011,
    color: 'black',
    price: 11000
  },
  {
    brand: "Toyota",
    model: "Yaris",
    year: 2002,
    color: 'yellow',
    price: 1100
  },
  {
    brand: "Toyota",
    model: "Avensis",
    year: 2014,
    color: 'blue',
    price: 10000
  },
];