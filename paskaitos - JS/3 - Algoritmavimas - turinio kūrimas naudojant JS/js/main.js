const tableData = document.querySelector('tbody.js-table-data');
const sortHeaders = document.querySelectorAll('.js-sort-header');

// functions
function renderTableRows() {
  tableData.innerHTML = '';
  cars.forEach(function (car) {
    tableData.innerHTML += `
    <tr>
      <td>${car.brand}</td>
      <td>${car.model}</td>
      <td>${car.year}</td>
      <td>${car.price}</td>
      <td>${car.color}</td>
    </tr>`;
  });
}

// event - tai addEventListener metodo siunčiamas parametras (informacinis objektas) apie 'eventą'

function handleSortOrder(event) {
  const arrowEl = assignArrowElement(event.target);
  const order = changeArrowState(arrowEl.classList);
  const sortCriteria = assignSortCriteria(event.target);
  sortItems(sortCriteria, order);
  renderTableRows();
}

function sortItems(criteria, order){
  const direction = order === 'DESC' ? -1 : 1;
  cars.sort(function (a, b) {
    switch (typeof a[criteria]) {
      case 'string': 
        return a[criteria].localeCompare(b[criteria])  * direction;
      case 'number':
        return (a[criteria] - b[criteria]) * direction;
      default:
        console.error('Palyginimo funkcija nemoka atlikti šio tipo palyginimo');
        return 0;
    }
  });
}

function changeArrowState(classList) {
  if (!classList.contains('sort-arrow--down')) {
    classList.add('sort-arrow--down');
    classList.remove('sort-arrow--up');
    return 'DESC';
  }
  classList.add('sort-arrow--up');
  classList.remove('sort-arrow--down');
  return 'ASC';
}

function assignArrowElement(target) {
  if (target.classList.contains('js-sort-header__arrow')) {
    return target;
  } else if (target.classList.contains('js-sort-header')) {
    return target.lastElementChild;
  }
  return target.nextElementSibling;
}

function assignSortCriteria(target) {
  if (target.classList.contains('js-sort-header__arrow')) {
    return target.previousElementSibling.innerHTML.toLowerCase();
  } else if (target.classList.contains('js-sort-header')) {
    return target.firstElementChild.innerHTML.toLowerCase();
  }
  return target.innerHTML.toLowerCase();
}

// Initial
renderTableRows();
sortHeaders.forEach(function (sortHeader) {
  sortHeader.addEventListener('click', handleSortOrder);
})




