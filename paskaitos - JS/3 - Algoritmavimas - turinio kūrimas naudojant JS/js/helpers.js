// 1 -> einamasis elementas yra aukštesnio prioriteto
// -1 ->  einamasis elementas yra žemesnio prioriteto
// 0 ->  einamasis elementas yra vienodo prioriteto
function byYearASC(a, b) {
  return a.year - b.year;
}
function byYearDESC(a, b) {
  return b.year - a.year;
}
function byPriceASC(a, b) {
  return a.price - b.price;
}
function byPriceDESC(a, b) {
  return b.price - a.price;
}
function byBrandASC(a, b) {
  return a.brand.localeCompare(b.brand);
}
function byBrandDESC(a, b) {
  return -1 * a.brand.localeCompare(b.brand);
}
function byModelASC(a, b) {
  return a.model.localeCompare(b.model);
}
function byModelDESC(a, b) {
  return -1 * a.model.localeCompare(b.model);
}
function byColorASC(a, b) {
  return a.color.localeCompare(b.color);
}
function byColorDESC(a, b) {
  return -1 * a.color.localeCompare(b.color);
}