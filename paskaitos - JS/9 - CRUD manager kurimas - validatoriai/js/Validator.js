class Validator {
  // Factory funkcija tai funkcija kuri formuoja funkcijas/objektus,
  // priklausomai nuo perduotų parametrų
  static createNumberInRangeValidator = (min, max) => {
    return {
      validate: value => {
        return value >= min && value <= max
      },
      errorMsg: `Number must be from ${min} to ${max}`
    }
  }

  static isStringValidator = {
    validate: value => typeof value === "string",
    errorMsg: "Input must be string"
  }

  static isNumberValidator = {
    validate: value => !isNaN(value),
  }


}

