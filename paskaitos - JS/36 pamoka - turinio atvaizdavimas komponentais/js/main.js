const root = document.querySelector('#app');

function addPaymentData(data) {
  data = data.reduce((dataObject, inputData) => ({
    ...dataObject,
    [inputData.name]: inputData.value
  }), {});

  const result = {
    fullname: data.fullname,
    profession: data.profession,
    contract: data.contract,
  };
  switch (data.contract) {
    case 'ds':
      result.salary = Number(data.salary);
      break;
    case 'iv':
      result.hourPay = Number(data.hourPay);
      result.hourCount = Number(data.hourCount);
    case 'dsn':
      result.pay = Number(data.pay);
      break;
  }
  paymentTable.addRow(result);
}

const addPaymentForm = new Form(addPaymentFormSettings, addPaymentData);
root.appendChild(addPaymentForm.render());


const paymentTable = new Table(paymentTableSettings, people);
root.appendChild(paymentTable.render());


