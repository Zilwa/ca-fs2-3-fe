class Select {
  constructor(options) {
    this.options = options;
  }

  render() {
    const select = document.createElement('select');
    select.className = 'form-select w-100 mb-3';

    this.options.forEach(({ title, value }) => {
      const option = document.createElement('option');
      option.setAttribute('value', value);
      option.innerHTML = title;
      select.appendChild(option);
    });

    return select;
  }
}