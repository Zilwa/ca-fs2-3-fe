class FormGroup {
  static characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  static fakeHash() {
    let result = '';
    for (let i = 0; i < 5; i++)
      result += FormGroup.characters.charAt(Math.floor(Math.random() * 6));
    return result;
  }

  constructor({ label, type, name, options }) {
    this.label = label;
    this.type = type;
    this.options = options;
    this.name = name
  }

  render() {
    const hash = FormGroup.fakeHash();
    const formGroup = document.createElement('div');
    formGroup.className = 'form-group';

    const label = document.createElement('label');
    label.innerHTML = this.label;
    label.setAttribute('for', hash + this.name);
    formGroup.appendChild(label);

    if (this.type === 'select') {
      const select = new Select(this.options).render();
      select.setAttribute('name', this.name);
      select.id = hash + this.name;
      formGroup.appendChild(select);
    } else {
      const input = new Input(this.type).render();
      input.setAttribute('name', this.name);
      input.id = hash + this.name;
      formGroup.appendChild(input);
    }
    return formGroup;
  }
}