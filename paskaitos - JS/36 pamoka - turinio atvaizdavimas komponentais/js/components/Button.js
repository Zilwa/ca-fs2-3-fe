class Button {
  constructor(text, className) {
    this.text = text;
    this.className = className || '';
  }

  render() {
    const button = document.createElement('button');
    button.className = 'btn btn-primary' + ( this.className
      ? ' ' + this.className
      : ''
    );
    button.innerHTML = this.text;
    return button;
  }
}