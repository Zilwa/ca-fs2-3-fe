import { HomeIcon } from 'FontAwsome/icons';

// Nauja versija
export default Button = ({ text, className }) => {
  return (
    <button className={'btn btn-primary' + className ? ' ' + className : ''}>
      <span><HomeIcon size="xl" /></span>
      <span>{text}</span>
    </button>
  )
}

// Sena versija
export default class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {text, className} = this.props;
    return (
      <button className={'btn btn-primary' + className? ' ' + className: ''}>
        <span><HomeIcon size="xl" /></span>
        <span>{text}</span>
      </button>
    )
  }
}