class Table {
  constructor(settings, data) {
    this.settings = settings;
    this.data = data;
    this.element = document.createElement('table');
    this.rowFormatFunctions = this.settings.map(({ callback }) => callback);  	
  }

  /**
 * Funkcijos paskirtis, suformuoti lentelės antraštę
 * 
 * @param {Array} headers - lentelės stulpelių pavadinimų masyvas
 */
  renderTableHead(headers) {
    const thead = document.createElement('thead');
    thead.className = 'bg-primary text-white text-uppercase fw-bold';
    const tr = document.createElement('tr');
    headers.forEach(colName => {
      const th = document.createElement('th');
      th.innerHTML = colName;
      tr.appendChild(th);
    })
    thead.appendChild(tr);
    return thead;
  }

  /**
   * Funkcijos paskirtis yra formuoti lentelės duomenų komponentą
   * 
   * @param {Array} data - eilutes atstovaujančių objektų masyvas
   */
  renderTableBody(data ) {
    const tbody = document.createElement('tbody');
    data.forEach(rowData => {
      const tr = document.createElement('tr');
      this.rowFormatFunctions.forEach(formInnerHTML => {
        const td = document.createElement('td');
        td.innerHTML = formInnerHTML(rowData);
        tr.appendChild(td);
      });
      tbody.appendChild(tr);
    });
    return tbody;
  }

  addRow(rowData){
    const tr = document.createElement('tr');
    this.rowFormatFunctions.forEach(formInnerHTML => {
      const td = document.createElement('td');
      td.innerHTML = formInnerHTML(rowData);
      tr.appendChild(td);
    });
    this.element.querySelector('tbody').appendChild(tr);
  }

  /**
   * Formuoja componentą
   *  <table>
   *    <thead>...</thead>  : renderTableHead(header)
   *    <tbody>...</tbody>  : renderTableBody(data, arrCols)
   *  </table>
   */
  render() {
    this.element.className = 'table table-striped w-75 mx-auto my-4 shadow';
    const thead = this.renderTableHead(this.settings.map(({ title }) => title));
    const tbody = this.renderTableBody(this.data)
    this.element.appendChild(thead);
    this.element.appendChild(tbody);
    return this.element;
  }
}