class Form {
  constructor({ header, formGroups, btnText }, callback) {
    this.header = header;
    this.formGroups = formGroups;
    this.btnText = btnText;
    this.callback = callback;
    // this.onSubmit = this.onSubmit.bind(this);
  }

  // onSubmit(e) {
  onSubmit = e => {
    // console.log(this);
    e.preventDefault();
    const inputs = Array.from(e.target)
      .filter(input => input.localName !== 'button');

    this.callback(inputs.map(input => ({
      name: input.name,
      value: input.value
    })));

    inputs.forEach(input => input.value = '');
  }

  render() {
    const form = document.createElement('form');
    form.className = 'w-75 mx-auto shadow p-3 mt-4';
    form.addEventListener('submit', this.onSubmit);

    const header = document.createElement('div');
    header.className = 'text-primary h2 my-4';
    header.innerHTML = this.header;
    form.appendChild(header);

    const row = document.createElement('div');
    row.className = 'row';
    form.appendChild(row);

    this.formGroups.forEach(formGroupData => {
      const col = document.createElement('div');
      col.className = "col-3";

      const formGroup = new FormGroup(formGroupData);
      col.appendChild(formGroup.render());

      row.appendChild(col);
    });

    const btnCol = document.createElement('div');
    btnCol.className = 'col-3';
    row.appendChild(btnCol);

    const btnParent = document.createElement('div');
    btnParent.className = 'd-flex align-items-end h-100';
    btnCol.appendChild(btnParent);

    const addBtn = new Button(this.btnText, 'w-100 d-block mb-3 fw-bold rounded-0');
    btnParent.appendChild(addBtn.render());

    return form;
  }
}