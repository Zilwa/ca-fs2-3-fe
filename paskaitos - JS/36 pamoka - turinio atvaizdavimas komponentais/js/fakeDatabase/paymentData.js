const people = [
  {
    id: 0,
    fullname: "Vergujus Klysta",
    profession: 'IT specialist',
    contract: 'ds',
    salary: 900,
  },
  {
    id: 1,
    fullname: "Svaigė Knisaitė",
    profession: 'Market flow analyst',
    contract: 'iv',
    hourPay: 30,
    hourCount: 80
  },
  {
    id: 5,
    fullname: "Dėžis Plokštainis",
    profession: "Server systems designer",
    contract: 'dsn',
    pay: 3600
  },
  {
    id: 6,
    fullname: "Skaita Kalkulytė",
    profession: 'Accountant',
    contract: 'ds',
    salary: 1700,
  },
  {
    id: 7,
    fullname: "Kėglis Bortauskas",
    profession: 'Environment architect',
    contract: 'iv',
    hourPay: 18,
    hourCount: 43
  },
  {
    id: 8,
    fullname: "Verslas Bankauskas",
    profession: 'Buisness analyst',
    contract: 'dsn',
    pay: 800
  },
  {
    id: 9,
    fullname: "Frontas Klikauskas",
    profession: 'Junior software developer',
    contract: 'ds',
    salary: 760,
  },
  {
    id: 10,
    fullname: "Haštė Papasas",
    profession: 'Software developer',
    contract: 'ds',
    salary: 1680,
  },
  {
    id: 11,
    fullname: "Klausas Planeklis",
    profession: 'Senior software developer',
    contract: 'ds',
    salary: 2350,
  },
  {
    id: 12,
    fullname: "Planas Planuoklis",
    profession: 'Software architect',
    contract: 'dsn',
    pay: 13000
  },
  {
    id: 13,
    fullname: "Senčius Vėpsa",
    profession: 'Guard',
    contract: 'ds',
    salary: 540,
  },
  {
    id: 45,
    fullname: "Vierchas Bankrotierius",
    profession: "CEO",
    contract: 'ds',
    salary: 1680,
  }
];