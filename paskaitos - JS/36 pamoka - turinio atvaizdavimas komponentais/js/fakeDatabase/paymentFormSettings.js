const addPaymentFormSettings = {
  header: 'Pridėti naują darbuotoją',
  formGroups: [
    {
      label: 'Pilnas Vardas',
      type: 'text',
      name: 'fullname'
    },
    {
      label: 'Specialybė',
      type: 'text',
      name: 'profession'
    },
    {
      label: 'Įdarbinimo pobūdis',
      type: 'select',
      name: 'contract',
      options: [
        { title: 'Darbo sutartis', value: 'ds' },
        { title: 'Individuali veikla', value: 'iv' },
        { title: 'Darbo santykiai', value: 'dsn' },
      ]
    },
    {
      label: 'Mėnesinis atlyginimas',
      type: 'text',
      name: 'salary'
    },
    {
      label: 'Valandinis įkainis',
      type: 'text',
      name: 'hourPay'
    },
    {
      label: 'Valandų skaičius',
      type: 'text',
      name: 'hourCount'
    },
    {
      label: 'Užmokestis',
      type: 'text',
      name: 'pay'
    },
  ],
  btnText: 'Pridėti'
}