class Input {
  constructor(type) {
    this.type = type;
  }

  render() {
    const input = document.createElement('input');
    input.setAttribute('type', this.type);
    input.className = 'form-control w-100 mb-3';
    return input;
  }
}