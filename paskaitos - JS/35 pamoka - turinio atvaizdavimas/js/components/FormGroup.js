class FormGroup {
  constructor(label, type, options) {
    this.label = label;
    this.type = type;
    this.options = options;
    console.log(label, type, options);
  }

  render() {
    const formGroup = document.createElement('div');
    formGroup.className = 'form-group';

    const label = document.createElement('label');
    label.innerHTML = this.label;
    formGroup.appendChild(label);

    if (this.type === 'select') {
      const select = new Select(this.options);
      formGroup.appendChild(select.render());
    } else {
      const input = new Input(this.type);
      formGroup.appendChild(input.render());
    }
    return formGroup;
  }
}