const formStructure  = {
  header: 'Pridėti naują darbuotoją',
  formGroups: [
    {
      label: 'Pilnas Vardas',
      type: 'text'
    },
    {
      label: 'Specialybė',
      type: 'text'
    },
    {
      label: 'Įdarbinimo pobūdis',
      type: 'select',
      options: [
        { title: 'Darbo sutartis', value: 'ds'},
        { title: 'Individuali veikla', value: 'iv'},
        { title: 'Darbo santykiai', value: 'dsn'},
      ]
    },
    {
      label: 'Mėnesinis atlyginimas',
      type: 'text'
    },
    {
      label: 'Valandinis įkainis',
      type: 'text'
    },
    {
      label: 'Valandų skaičius',
      type: 'text'
    },
    {
      label: 'Užmokestis',
      type: 'text'
    },
  ]
}