class Table {
  constructor(settings, data) {
    this.settings = settings;
    this.data = data;
  }

  /**
 * Funkcijos paskirtis, suformuoti lentelės antraštę
 * 
 * @param {Array} headers - lentelės stulpelių pavadinimų masyvas
 */
  renderTableHead(headers) {
    const thead = document.createElement('thead');
    thead.className = 'bg-primary text-white text-uppercase fw-bold';
    const tr = document.createElement('tr');
    headers.forEach(colName => {
      const th = document.createElement('th');
      th.innerHTML = colName;
      tr.appendChild(th);
    })
    thead.appendChild(tr);
    return thead;
  }

  /**
   * Funkcijos paskirtis yra formuoti lentelės duomenų komponentą
   * 
   * @param {Array} data - eilutes atstovaujančių objektų masyvas
   * @param {Array} arrCols - stulpelių formavimo funkcijų masyvas
   */
  renderTableBody(data, arrCols) {
    const tbody = document.createElement('tbody');
    data.forEach(rowData => {
      const tr = document.createElement('tr');
      arrCols.forEach(formInnerHTML => {
        const td = document.createElement('td');
        td.innerHTML = formInnerHTML(rowData);
        tr.appendChild(td);
      });
      tbody.appendChild(tr);
    });
    return tbody;
  }

  /**
   * Formuoja componentą
   *  <table>
   *    <thead>...</thead>  : renderTableHead(header)
   *    <tbody>...</tbody>  : renderTableBody(data, arrCols)
   *  </table>
   */
  render() {
    const table = document.createElement('table');
    table.className = 'table table-striped w-75 mx-auto my-4 shadow';
    const thead = this.renderTableHead(this.settings.map(({ title }) => title));
    const tbody = this.renderTableBody(this.data, this.settings.map(({ callback }) => callback))
    table.appendChild(thead);
    table.appendChild(tbody);
    return table;
  }
}