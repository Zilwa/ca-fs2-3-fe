const root = document.querySelector('#app');
const paymentTableSettings = [
  {
    title: 'Pilnas vardas',
    callback: ({ fullname }) => fullname || '—'
  },
  {
    title: 'Specialybė',
    callback: ({ profession }) => profession || '—'
  },
  {
    title: 'Įdarbinimo pobūdis',
    callback: ({ contract }) => contract === 'ds'
      ? 'Darbo sutartis'
      : contract === 'dsn'
        ? 'Darbo santykiai'
        : 'Individuali veikla'
  },
  {
    title: 'Mėnesinis atlyginimas',
    callback: ({ salary }) => salary ? salary + " €" : '—'
  },
  {
    title: 'Valandinis įkainis',
    callback: ({ hourPay }) => hourPay ? hourPay + " €" : '—'
  },
  {
    title: 'Valandų skaičius',
    callback: ({ hourCount }) => hourCount || '—'
  },
  {
    title: 'Užmokestis',
    callback: ({ pay }) => pay ? pay + " €" : '—'
  },
  {
    title: 'Viso',
    callback: person => (person.contract === 'ds'
      ? person.salary
      : person.contract === 'dsn'
        ? person.pay
        : person.hourCount * person.hourPay) + " €"
  },
];

const paymentTable = new Table(paymentTableSettings, people);
const formGroup1 = new FormGroup(
  'Įdarbinimo pobūdis',
  'select',
  [
    { title: 'Darbo sutartis', value: 'ds'},
    { title: 'Individuali veikla', value: 'iv'},
    { title: 'Darbo santykiai', value: 'dsn'},
  ]
);
const formGroup2 = new FormGroup('Specialybė', 'text');

root.appendChild(paymentTable.render());
root.appendChild(formGroup1.render());
root.appendChild(formGroup2.render());


