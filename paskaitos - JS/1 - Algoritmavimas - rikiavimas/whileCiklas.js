'use strict';

const numbers = [1, 7, 8, 9, 4, 1, 2, 5, 6, 9, 8];

function bubbleSort(array) {
  const arr = [...array];
  let i = 0, j;
  while (i < arr.length) {
    j = 0;
    while (j < arr.length - 1 - i) {
      if (arr[j] > arr[j + 1]) {
        let temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
      j++;
    }
    i++;
  }
  return arr;
}

function insertionSort(array) {
  const arr = [...array];
  let i = 1, j;
  while (i < arr.length) {
    if (arr[i] < arr[i - 1]) {
      j = i;
      while (arr[j] < arr[j - 1]) {
        let temp = arr[j];
        arr[j] = arr[j - 1];
        arr[j - 1] = temp;
        j--;
      }
    }
    i++;
  }
  return arr;
}

const sortedBubble = bubbleSort(numbers);
const sortedInsertion = insertionSort(numbers);
numbers.push(777);
console.log('Išrikiuota naudojant bubble sort algoritmą');
console.log(sortedBubble);
console.log('Išrikiuota naudojant insertion sort algoritmą');
console.log(sortedInsertion);

