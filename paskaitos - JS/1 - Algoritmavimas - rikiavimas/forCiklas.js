'use strict';
// Praktinis ciklo cikle panaudojimas

const numbers = [1, 7, 8, 9, 4, 1, 2, 5, 6, 9, 8];

function bubbleSort(arr) {
  console.log('Buble sort funkcijos pradžia');
  console.log(arr);
  // Išorinio ciklo tikslas yra su kiekviena iteracija mažinti duomenų imtį
  // Vidinio ciklo tikslas yra nustumti didžiausią skaičių į masyvo duomenų imties galą
  //        Pradinis iteratorius - i. Tai kintamasis, kuris kinta po kiekvienos iteracijos
  //        ↓                       ↓ - Žingsnis, tai iteratoriaus kitimas taip, jog jis artėtų link baigtinės salygos
  for (let i = 0; i < arr.length; i++) {
    //                 ↑↑↑ - Testinumo salyga
    for (let j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        // dviejų masyvo elementų reikšmių apkeitimas vietomis 
        let temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
  return arr;
}

function insertionSort(arr) {
  console.log('insretion sort funkcijos pradžia');
  console.log(arr);
  // Išorinio ciklo tikslas surasti elementą, kuris būtų mažesnis nei vienu indeksu mažesnis
  // Vidinio ciklo tikslas yra stumti elementą į kairę pusę, TOL kol jis yra mažesnis už elementą, mažesniu vienu indeksu
  for (let i = 1; i < arr.length; i++) {
    if (arr[i] < arr[i - 1]) {
      for (let j = i; arr[j] < arr[j - 1]; j--) {
        let temp = arr[j];
        arr[j] = arr[j - 1];
        arr[j - 1] = temp;
      }
    }
  }
  return arr;
}
const sortedBubble = bubbleSort(numbers);
const sortedInsertion = insertionSort(numbers);
numbers.push(777);
console.log('Išrikiuota naudojant bubble sort algoritmą');
console.log(sortedBubble);
console.log('Išrikiuota naudojant insertion sort algoritmą');
console.log(sortedInsertion);

// 1. Perrašyti funkcijas taip, jog grąžintų masyvą naujoje atmintyje ir nepakeistų pradinių duomenų
// 2. For ciklus pakeisti While ciklais
