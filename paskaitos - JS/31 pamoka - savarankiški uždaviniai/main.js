'use strict';
//  Užduotys iš šios nuorodos:
// https://www.w3resource.com/javascript-exercises/javascript-basic-exercises.php
// Namų darbas: išspręsti 1 uždavinį
// Atlikti: 1, 3, 4, 6, 7, 8, 9, 11, 12, 13, 15, 16, 17, 18, 21, 22, 23, 24, 25, 31, 32, 33, 35, 36, 41

// 1.
{
  const today = new Date();
  const day = today.getDay(); // 0 - 6
  const daylist = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  const hours24 = today.getHours();
  const hours = hours24 > 12 ? hours24 - 12 + ' PM' : hours24 + ' AM';
  const minute = today.getMinutes();
  const second = today.getSeconds();
  console.groupCollapsed('1.')
  console.log(`Today is : ${daylist[day]}.`);
  console.log(`Current time is : ${hours} : ${minute} : ${second}`);
  console.groupEnd();
}

// 3.
{
  const date = new Date();
  const year = date.getFullYear();
  const monthNumber = date.getMonth() + 1;
  const month = monthNumber < 10 ? '0' + monthNumber : monthNumber;
  const dayNumber = date.getDate();
  const day = dayNumber < 10 ? '0' + dayNumber : dayNumber;
  console.groupCollapsed('3.')
  console.log(`${month}-${day}-${year}`);
  console.log(`${month}/${day}/${year}`);
  console.log(`${day}-${month}-${year}`);
  console.log(`${day}/${month}/${year}`);
  console.groupEnd();
}

// 4.
{
  function triangleArea(a, b, c) {
    // p - pusperimetris
    const p = (a + b + c) / 2;
    return Math.sqrt(p * (p - a) * (p - b) * (p - c));
  }
  const examplesString = [
    [5, 6, 7], // 0 - data
    [6, 6, 6], // 1 - data
    [3, 4, 5], // 2 - data
  ].reduce((str, data) => {
    const call = `triangleArea([${data.join(', ')}])`;
    const answer = triangleArea(...data).toFixed(2); // ...data ⇨ data[0], data[1], data[2]
    return str + `${call} ⇨ ${answer}\n`;
  }, '')
  console.groupCollapsed('4.')
  console.log(examplesString);
  console.groupEnd();
}


// 6.
{
  // Leap year occurs in every year that is divisible by four, but only in century years that are evenly divided by 400
  function isLeapYear(year) {
    return Math.floor(year / 100) % 4 === 0 && year % 4 === 0
  }
  const examplesString = [
    2020,
    1600,
    1993,
    4024,
  ].reduce((str, year) => {
    const call = `isLeapYear(${year})`;
    const answer = isLeapYear(year);
    return str + `${call} ⇨ ${answer}\n`;
  }, '')

  console.groupCollapsed('6.')
  console.log(examplesString);
  console.groupEnd();
}


// 7.
{
  console.groupCollapsed('7. Sausio pirmos sekmadieniai nuo 2014 iki 2050');
  const sundayYears = [];
  for (let year = 2014; year <= 2050; year++) {
    const date = new Date();
    date.setFullYear(year); // Metai
    date.setMonth(0); // Sausis
    date.setDate(1); // Pirma
    if (date.getDay() === 0)  // 0 - sekmadienis
      sundayYears.push(year);
  }
  console.log(sundayYears);
  console.groupEnd();
}

// 8.
{
  /*
  console.groupCollapsed('8. Atspėk skaičių');
  let input;
  const random = Math.floor(Math.random() * 10) + 1;
  console.log(`P.S.: skaičius yra ${random}, jeigu ką`);
  do {
    if (input === undefined) input = prompt('Sveiki!\nĮveskite Skaičių nuo 1 iki 10...');
    else input = prompt('Prašome neraganėti ir įvesti skaičių, jog būtų galima parodyt rankos pirštais.🤨');
  } while (isNaN(input) || !(Number(input) >= 0 && Number(input) <= 10));
  if (random === Number(input)) alert('Sveikinu atspėjus skaičių, tu labai protingas 🏁');
  else alert('Nepavyko, sori.');
  console.groupEnd();
  */
}

// 9.
{
  console.groupCollapsed('9. Dienos iki Kalėdų');
  function getDaysInMonth(month, year) {
    return new Date(year, month + 1, 0).getDate();
  };

  const date = new Date();
  date.setFullYear(2021);
  date.setMonth(0);
  date.setDate(2);
  let days = 0;
  if (date.getMonth() === 11 && date.getDate() >= 25) {
    console.log('kitais metais')
    days += getDaysInMonth(date.getMonth(), date.getFullYear()) - date.getDate();
    const nextYearDate = new Date();
    nextYearDate.setFullYear(date.getFullYear() + 1);
    nextYearDate.setMonth(0);
    nextYearDate.setDate(1);
    for (let month = nextYearDate.getMonth(); month <= 10; month++)
      days += getDaysInMonth(month, nextYearDate.getFullYear());
    days += 25;
  }
  else {
    if (date.getMonth() === 11) {
      days += 25 - date.getDate();
    } else {
      days += getDaysInMonth(date.getMonth(), date.getFullYear()) - date.getDate();
      for (let month = date.getMonth() + 1; month <= 10; month++)
        days += getDaysInMonth(month, date.getFullYear());
      days += 25;
    }
  }

  console.log(`Iki Kalėdų liko: ${days}`);
}

