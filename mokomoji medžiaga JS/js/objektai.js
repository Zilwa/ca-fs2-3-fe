'use strict';

let automobilis = {
  marke: 'Opel',
  modelis: 'Astra',
  metai: 2005,
  spalva: 'Juoda',
  variklioTuris: 2.0,
  dauzta: true,
  kuroTipas: 'dyzelis'
};

// Savybių panaudojimas
console.log(automobilis.marke + ' ' + automobilis.modelis);
console.log(automobilis['marke'] + ' ' + automobilis['modelis']);

// Savybių kūrimas/perrašymas
automobilis.modelis = 'Vectra'; console.log(automobilis);
automobilis['spalva'] = 'Raudona'; console.log(automobilis);

// Primitive type vs Reference type
console.log('Kopijuojant primityvaus tipo kintamuosius, yra išskiriama nauja atminties vieta. Kintamieji rodo į atskiras atminties vietas.')
let a = 11;
let b = a;
a = 15;
console.log('a:', a, 'b:', b);

console.log('Kopijuojant nuorodos tipo kintamuosius, nauja atminties vieta NĖRA išskiriama, o vietoje to yra sukuriama nuoroda į tą pačią atminties vietą.');
let c = {
  num: 11
};
let d = c;
c.num = 15;
console.log('c.num:', c.num, 'd.num:', d.num);

// Nuorodos tipo kitamųjų reikšmių kopijavimas.
// Norint pakopijuoti nuorodps tipo kintamąjį taip, jog jis turėtų savo atskirą atminties vietą, naudoti šią metodologiją:
let e = JSON.parse(JSON.stringify(d));
d.num = 99;
e.num = -2;
console.log('c.num:', c.num, 'd.num:', d.num, 'e.num:', e.num);