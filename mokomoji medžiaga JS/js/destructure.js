const settings = {
  name: 'Web App',
  mode: 'development',
  hasFramework: false,
  libraries: ['vue', 'jquery', 'popper.js'],
  entry: 'index/main.js'
}

const sitePages = [
  {
    title: 'Home',
    showAdds: false,
    authorized: false,
    seoTracking: true
  },
  {
    title: 'About',
    showAdds: true,
    authorized: false,
    seoTracking: true
  },
  {
    title: 'Dashboard',
    showAdds: false,
    authorized: true,
    seoTracking: false
  }
]

// Destruktūrizavimas tai tik dalies informacijos išsitraukimas iš objekto ar masyvo

// Objekto destruktūrizavimas:
const {name, mode} = settings;
// standartine alternatyva
// const name = settings.name;
// const mode = settings.mode;

// Objekto destruktūrizavimas funkcijos argumentuose
function printPageInfo({title, showAdds, authorized, seoTracking}){
  document.querySelector('body').innerHTML += `
    <div>${title}</div>
    <div>${showAdds}</div>
    <div>${authorized}</div>
    <div>${seoTracking}</div>
  `;
}
sitePages.forEach(printPageInfo);

// Masyvo destruktūrizavimas:
const [pirmasPagalAbecele, antrasPagalAbecele] = sitePages.sort((a, b) => a.title.localeCompare(b.title));
// standartine alternatyva
// const pirmasPagalAbecele = sitePages[0];
// const antrasPagalAbecele = sitePages[1];

console.log({pirmasPagalAbecele, antrasPagalAbecele});

// Spread operatorius < ... >
// ...objektas - išrašo visas objekto savybes atskirai (SU SAVYBIŲ PAVADINIMAIS)
const allSettings = {
  setting1: 'asdasd',
  setting2: true,
  ...settings
}
console.log(allSettings);

// ...masyvas - išrašo visas masyvo reikšmes atskirai
console.log(sitePages[0], sitePages[1], sitePages[2]);
console.log(...sitePages);
