console.log('-------------------prototipai.js--------------------------');
// masyvo prototipas
// prototipas tai bendrinė struktūra (šablonas) pagal kurį kuriami objektai
// P.S.: Labai panašu į klases objektiniame programavime
const arr = [1, 2, 3];
console.dir(arr);

// Prototipo kūrimas, išskiriant nauja atmintį
// Prototipai kuriami naudojant "gamyklinę" (factory) funkciją
function Car(brand, model, year) {
  // Prototype properties - savybės
  this.brand = brand;
  this.model = model;
  this.year = year;
  // Prototipo metodai kuriami viduje 'factory' funkcijos
  this.__proto__.showFullName = function () {
    console.log(this.brand + ' ' + this.model);
  };
}

// Prototipo papildymas 'factory' funkcijos išorėje
new Car().__proto__.showAge = function () {
  console.log(new Date().getFullYear() - this.year);
}

// new - išskiriama nauja atminties vieta, su kuria bus kviečiama funkcija
// toji naujai sukurta atminties vieta funkcijos viduje bus pasiekiama žodžiu this
const myCar = new Car("Peugeot", "406 Coupe", 1998);
console.log(myCar);
myCar.showFullName();
myCar.showAge();

const mySistersCar = new Car("Skoda", "T100", 2010);
console.log(mySistersCar);
mySistersCar.showFullName();
mySistersCar.showAge();
console.log('-------------------prototipai.js--------------------------');
