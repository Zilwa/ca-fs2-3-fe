// 1
class Flat {
  constructor(floor, price, address, ownerNumber, city, roomSquares) {
    this.floor = floor;
    this.price = price;
    this.address = address;
    this.ownerNumber = ownerNumber;
    this.city = city;
    this.roomSquares = roomSquares;
  }
  // 2
  countTotalSquares = () => this.roomSquares.reduce((acc, el) => acc + el, 0);
}
// 1.1
const flats = [
  new Flat(1, 55000, 'adresas', '+370 615 98745', 'Kaunas', [29, 19, 13]),
  new Flat(3, 60000, 'adresas', '+370 625 98745', 'Vilnius', [23, 17, 19]),
  new Flat(2, 50000, 'adresas', '+370 635 98745', 'Vilnius', [28, 15, 18]),
  new Flat(3, 47000, 'adresas', '+370 645 98745', 'Alytus', [25, 15, 18]),
  new Flat(3, 40000, 'adresas', '+370 655 98745', 'Vilnius', [22, 16, 18]),
  new Flat(5, 46000, 'adresas', '+370 665 98745', 'Vilnius', [21, 14, 18]),
];
// 3
flats.forEach(flat => console.log(flat.countTotalSquares()));
// 4
const flats2To4 = flats.filter(({ floor }) => floor >= 2 && floor <= 4);
// 5
const flats2To4Vilnius = flats2To4.filter(({ city }) => city === 'Vilnius');
// 6
flats2To4Vilnius.sort((a, b) => a.price - b.price);
// 7
const avg = flats2To4Vilnius.reduce((acc, { price }) => acc + price / flats2To4Vilnius.length, 0);
// 8
const flatsInfo = flats2To4Vilnius.map(flat => ({
  number: flat.ownerNumber,
  price: flat.price,
  address: flat.address,
  squares: flat.countTotalSquares()
}));
//
console.table(flatsInfo);