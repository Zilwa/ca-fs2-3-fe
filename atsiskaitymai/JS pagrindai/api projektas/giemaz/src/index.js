import "./scss/main.scss";
const pokemons_container = document.getElementById("pokemon_container");
const pokemons_number = 120;
const colors = {
  fire: "#F08030",
  grass: "#78C850",
  electric: "#F8D030",
  water: "#6890F0",
  ground: "#E0C068",
  rock: "#d5d5d4",
  fairy: "#fceaff",
  poison: "#A040A0",
  bug: "#f8d5a3",
  dragon: "#97b3e6",
  psychic: "#eaeda1",
  flying: "#A890F0",
  fighting: "#C03028",
  normal: "#F5F5F5",
};

// ==================API=======================
// reikalingas iskiepis i webpack "npm install --save @babel/polyfill"
//  ir webpack.common faile entry pakeist i "["@babel/polyfill", "./src/index.js"]"
const fetchPokemons = async () => {
  for (let i = 1; i <= pokemons_number; i++) {
    await getPokemon(i);
  }
};

const getPokemon = async (id) => {
  const url = `https://pokeapi.co/api/v2/pokemon/${id}`;
  const res = await fetch(url);
  const pokemon = await res.json();
  createPokemonCard(pokemon);
};

// =================Pokemon Card===============
function createPokemonCard(pokemon) {
  const pokemonEl = document.createElement("div");
  pokemonEl.classList.add("pokemon");
  // pora funkciju kurios padeda atfiltruoti ir pasirinkti pagrindini pokemono tipa is keliu esamu
  const main_types = Object.keys(colors);
  const poke_types = pokemon.types.map((el) => el.type.name);
  const type = main_types.find((type) => poke_types.indexOf(type) > -1);

  const name = pokemon.name[0].toUpperCase() + pokemon.name.slice(1);
  // korteles fono spalva tokia kaip ir tipas
  const color = colors[type];
  pokemonEl.style.backgroundColor = color;
  // numerio atvaizdavimui  padidinat symboliu kieki panaudota gamyklines toString ir padStart() funkcijos
  const pokeInnerHTML = `
        <div class="pokemon_img-container">
            <img src="https://pokeres.bastionbot.org/images/pokemon/${
              pokemon.id
            }.png" alt="${name}" />
        </div>
        <div class="pokemon_info">
            <span class="pokemon_number">#${pokemon.id.toString().padStart(3, "0")}</span>
            <h3 class="pokemon_name">${name}</h3>
            <small class="type">Type: <span>${type}</span></small>
        </div>
    `;

  pokemonEl.innerHTML = pokeInnerHTML;
  pokemons_container.appendChild(pokemonEl);
}
// ========================Card end======================
fetchPokemons();
