const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  // entry: './src/index.js',
  entry: ["@babel/polyfill", "./src/index.js"],
  output: {
    filename: "main.[contenthash].js",
    path: path.resolve(__dirname, "dist"),
    publicPath: "",
    assetModuleFilename: "images/[hash][ext]",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
            plugins: [
              "@babel/plugin-syntax-dynamic-import",
              "@babel/plugin-proposal-class-properties",
            ],
          },
        },
      },
      {
        test: /\.scss$/i,
        exclude: /node_modules/,
        use: [
          // 3. Perkeliama į failą, pagal MiniCssExtractPlugin plugin'o nustatymus
          MiniCssExtractPlugin.loader,
          // 2. s[ac]ss paverčiamas į css
          "css-loader",
          // 1. Galite importuoti .s[ac]ss failus Javascripté. Nuskaitomi s[ac]ss failai.
          "sass-loader",
        ],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|jfif)$/i,
        type: "asset/resource",
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      linkType: "text/css",
      filename: "[name].[contenthash].css",
    }),
  ],
};
