const getData = (callback) => {
  fetch('https://randomuser.me/api/?results=20')
  .then(response => response.json())
  .then(data => callback(data.results));
}

export default {
  getData
}