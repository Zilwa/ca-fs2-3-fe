import UserCard from './components/UserCard';
import api from './api';
import './scss/main.scss';


const root = document.querySelector('#app');

let
  userCards = [],
  cardContainer;

cardContainer = document.createElement('div');
cardContainer.className = 'card-container';


const initUserLoad = users => {
  root.appendChild(cardContainer);
  mountUsers(users);
  setTimeout(() => cardContainer.classList.add('show'), 0);
}

const mountUsers = users => {
  const newUserCards = users.map(userData => new UserCard(userData));
  newUserCards.forEach(userCard => cardContainer.appendChild(userCard.mount()));
  userCards = [...userCards, ...newUserCards];
}


api.getData(initUserLoad);