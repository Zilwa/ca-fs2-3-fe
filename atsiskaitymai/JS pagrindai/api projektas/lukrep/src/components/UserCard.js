export default class UserCard {
  static requiredProps = [
    'name',
    'email',
    'login',
    'dob',
    'location',
    'nat',
    'picture',
  ];

  constructor(props) {
    this.element = document.createElement('article');
    this.element.className = 'user-card';
    UserCard.requiredProps.forEach(
      prop => this[prop] = props[prop] || props[prop] === 0
        ? props[prop]
        : ''
    );
  }


  mount() {
    this.element.innerHTML = `
    <div class="user-card__div">
      <div>
      <img 
        class="user-card__img" src="${this.picture.large}" 
      />
      </div>
      <h2 class="user-card__header">
        <a href="#">${this.name.title} ${this.name.first} ${this.name.last}</a>
      </h2>
      <div class="user-card__about">
        <p>
          Hi, My name is ${this.name.first} ${this.name.last}. My username is ${this.login.username}. I'm ${this.dob.age} years old and I live in ${this.location.city}, ${this.nat}.
        </p>
      </div>
    </div>
    `;
    return this.element;
  }
}