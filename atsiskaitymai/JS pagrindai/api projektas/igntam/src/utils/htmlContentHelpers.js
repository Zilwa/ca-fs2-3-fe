const formHtmlStructure = (content, maxContentSymbols) => {
  const
    root = { el: 'root', children: [] };

  let
    res = content,
    contentSymbols = 0,
    i = 0,
    currentElement = root,
    currentAttribute = null,
    readingElementClosing = false,

    elementName = '',
    readingElementName = false,

    attributeName = '',
    readingAttributeName = false,

    attributeValue = '',
    readingAttributeValue = false;


  while (contentSymbols < maxContentSymbols && i < res.length) {
    // TODO: keisti 'currentElement' nuorodą ir sukurti elementuose nuorodą į tėvą
    // Laukiame Elemento Uždarymo
    if (readingElementClosing) {
      if (res[i] === '>') {
        readingElementClosing = false;
        currentElement.closingTag.closed = true;
        currentElement = currentElement.parentElement;
      }
    }
    // Attributo reikšmės nuskaitinėjimas
    else if (readingAttributeValue) {
      if (res[i] === '>' || res[i] === ' ') {
        currentAttribute.value = attributeValue;
        readingAttributeValue = false;
        attributeValue = '';
        if (res[i] === '>') currentElement.openingTag.closed = true;
        else {
          readingAttributeName = true;
          currentAttribute = {
            name: '',
            value: ''
          };
          currentElement.attributes.push(currentAttribute)
        }
      } else {
        attributeValue += res[i];
      }
    }
    // Attributo pavadinimo nuskaitinėjimas
    else if (readingAttributeName) {
      if (res[i] === '=') {
        currentAttribute.name = attributeName;
        readingAttributeName = false;
        attributeName = ''
        readingAttributeValue = true
      } else {
        attributeName += res[i];
      }
    }
    // Elemento atidarymo nuskaitinėjimas
    else if (readingElementName) {
      if (res[i] === ' ' || res[i] === '>') {
        currentElement.el = elementName;
        elementName = '';
        readingElementName = false;
        if (res[i] === '>') currentElement.openingTag.closed = true;
        else {
          readingAttributeName = true;
          currentAttribute = {
            name: '',
            value: ''
          };
          currentElement.attributes.push(currentAttribute)
        }
      } else {
        elementName += res[i];
      }
    }
    // Specialiųjų simbolių paieška
    else if (res[i] === '<' && res[i + 1] !== '/') {
      const child = {
        el: '',
        parentElement: currentElement
      };
      currentElement.children.push(child)
      currentElement = child;
      readingElementName = true;
      currentElement.openingTag = {
        opened: true,
        closed: false
      }
      currentElement.closingTag = {
        opened: false,
        closed: false
      }
      currentElement.children = [];
      currentElement.attributes = [];
    } else if (res[i] === '/' && res[i - 1] === '<') {
      currentElement.closingTag.opened = true;
      readingElementClosing = true;
    }

    i++;
    if (
      !readingElementClosing &&
      !readingElementName &&
      !readingAttributeName &&
      !readingAttributeValue
    ) contentSymbols++;
  }
  res = res.slice(0, i);
  return {
    slicedContent: res,
    htmlStructure: root.children,
    contentSymbolCount: contentSymbols
  }
}


export {
  formHtmlStructure
}