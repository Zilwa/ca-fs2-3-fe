function getRandomDog(callback) {
	fetch('https://random.dog/woof.json')
		.then(res => res.json())
		.then(data => callback(data.url))
		.catch(error => console.error(
			`Klaida:\n\tapi/index.js, getUsers(...).\nNepavyko gauti teigiamo atsakymo:\n\t${error}`
			));
}

export default {
    getRandomDog
  }