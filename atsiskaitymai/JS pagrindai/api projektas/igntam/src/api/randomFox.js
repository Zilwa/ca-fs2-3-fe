function getRandomFox(callback) {
	fetch('https://randomfox.ca/floof/')
		.then(res => res.json())
		.then(data => callback(data.image))
	    .catch(error => console.error(
		    `Klaida:\n\tapi/index.js, getUsers(...).\nNepavyko gauti teigiamo atsakymo:\n\t${error}`
		    ));
}

export default {
    getRandomFox
  }