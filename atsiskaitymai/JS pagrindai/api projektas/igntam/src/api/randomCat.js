function getRandomCat(callback) {
	fetch('https://aws.random.cat/meow')
		.then(res => res.json())
		.then(data => callback(data.file))
		.catch(error => console.error(
				`Klaida:\n\tapi/index.js, getUsers(...).\nNepavyko gauti teigiamo atsakymo:\n\t${error}`
				));
}

export default {
    getRandomCat
  }