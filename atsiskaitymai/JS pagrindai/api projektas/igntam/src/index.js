import './scss/main.scss';
import cats from './api/randomCat';
import dogs from './api/randomDog';
import foxes from './api/randomFox';

const mainApp = document.querySelector('#app');

const btns = document.createElement('div');
btns.className = 'buttons'

const cat__btn = document.createElement('button');
cat__btn.id = 'cat_btn';
cat__btn.innerHTML = `Get Cat`;
const dog__btn = document.createElement('button');
dog__btn.id = 'dog_btn';
dog__btn.innerHTML = `Get Dog`;
const fox__btn = document.createElement('button');
fox__btn.id = 'fox_btn';
fox__btn.innerHTML = `Get Fox`;

let results = document.createElement('div');
results.className = 'results'

let cat__result =document.createElement('div');
cat__result.id = `cat_result`;
cat__result.className = `result`
cat__result.innerHTML = `<p>Random Cat Placeholder`

let dog__result =document.createElement('div');
dog__result.id = `dog_result`;
dog__result.className = `result`
dog__result.innerHTML = `<p>Random Dog Placeholder`

let fox__result =document.createElement('div');
fox__result.id = `fox_result`;
fox__result.className = `result`
fox__result.innerHTML = `<p>Random Fox Placeholder`


results.appendChild(cat__result);
results.appendChild(dog__result);
results.appendChild(fox__result);
btns.appendChild(cat__btn);
btns.appendChild(dog__btn);
btns.appendChild(fox__btn);
mainApp.appendChild(results);
mainApp.appendChild(btns);


const catLoader = users => {
	cat__result.innerHTML = `<img src=${users} alt="cat" />`
  }

const dogLoader = (users) => {
	users.includes('.mp4') 
	? dogs.getRandomDog(dogLoader)
	: dog__result.innerHTML = `<img src=${users} alt="dog" />`
  }

const foxLoader = (users) => {
	fox__result.innerHTML = `<img src=${users} alt="fox" />`;
  }

cat__btn.addEventListener('click', function () {cats.getRandomCat(catLoader)});
dog__btn.addEventListener('click', function () {dogs.getRandomDog(dogLoader)});
fox__btn.addEventListener('click', function () {foxes.getRandomFox(foxLoader)});









