## Required programs
node.js -v above 10.4
git bash
## To install libraries run command in current folder:
npm i
## To start development mode in hot server run command in current folder:
npm start
## To start compile files in production mode run command:
npm run build
Liudvikas Natalevičius
Giedrius Mažeika