import critics_pick from '../assets/img/critics_pick.png';

const criticsPickCheckbox = document.createElement('div');
criticsPickCheckbox.innerHTML = `
<label class="checkbox-group" for="checkbox">
    <input id="checkbox" type="checkbox" />
    <span >View Critics' picks only</span>
    <img class="critics-pick-icon" for="checkbox" src="${critics_pick}"/>
</label>
`;

export default criticsPickCheckbox;