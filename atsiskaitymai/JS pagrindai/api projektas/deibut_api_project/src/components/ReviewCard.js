import criticsPickImg from '../assets/img/critics_pick.png';
import emptyMultimediaImg from '../assets/img/empty_multimedia_img.jpg';

// Žilvinai, norėjau šią checkIsChecked() išsikelti prie helperių, bet strigau, nesugebėjau importuoti į ReviewCard.js :).
let filter;
const checkIsChecked = () => {
    const criticsPickCheckbox = document.getElementById('checkbox');
    criticsPickCheckbox.addEventListener('change', (event) => {
        let elements;
        if (event.target.checked) {
            elements = document.querySelectorAll('.non-critics-pick').forEach(el => el.classList.add('non-critics-pick--hidden'));
            filter = 'pick';
        } else {
            elements = document.querySelectorAll('.non-critics-pick').forEach(el => el.classList.remove('non-critics-pick--hidden'));
            filter = 'all';
        }
    });
}

export { checkIsChecked }

export default class ReviewCard {
    static requiredProps = [
        'byline',
        'critics_pick',
        'date_updated',
        'display_title',
        'headline',
        'link',
        'mpaa_rating',
        'multimedia',
        'summary_short',
    ]

    constructor(props) {
        this.element = document.createElement('article');
        this.element.classList = 'review-card';
        ReviewCard.requiredProps.forEach(prop => this[prop] = props[prop] || props[prop] === 0
            ? props[prop]
            : ''
        );
    }

    mountOneCard() {
        const img = this.critics_pick === 1 ? criticsPickImg : '';
        this.critics_pick === 0 ? this.element.classList.add('non-critics-pick') : '';
        const emptyMultimedia = emptyMultimediaImg;
        const multimedia = this.multimedia === '' ? emptyMultimedia : this.multimedia.src;
        checkIsChecked();
        filter === 'pick' && this.element.classList.contains('non-critics-pick') ? this.element.classList.add('non-critics-pick--hidden') : '';
        this.element.innerHTML = `
        <header class="review-card__header">
            <div class="review-card__header__title">
                <h2>${this.display_title}</h2>
            </div>
            <div>
                <img class="review-card__header__critics-pick-icon" src="${img}"/>
            </div>
        </header>
        <img class="review-card__multimedia" src="${multimedia}"/>
        <a class="review-card__link" href="${this.link.url}" target="blank"><h3 class="review-card__headline">${this.headline}</h3></a>
        <div class="review-card__byline-and-separator-and-date-updated">
            <div class="review-card__byline">By ${this.byline}</div>
            <div class="review-card__separator"> &nbsp &nbsp | &nbsp &nbsp </div>
            <div class="review-card__date-updated">${this.date_updated}</div>
        </div>
        <p class="review-card__summary-short">${this.summary_short}</p>
        <div class="review-card__space-for-span"></div>
        <span class="review-card__mpaa-rating">${this.mpaa_rating}</span>
        `;
        return this.element;
    }
}