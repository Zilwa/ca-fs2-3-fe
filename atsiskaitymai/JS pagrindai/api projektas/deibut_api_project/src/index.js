import api from './api/api'; 
import ReviewCard from './components/ReviewCard';
import criticsPickCheckbox from './components/criticsPickCheckbox';
import { checkIsChecked } from './components/ReviewCard';
import "./scss/main.scss";

const 
root = document.querySelector('#app'),
portion = 20;

let 
reviewCards = [],
fetchCycles = 1,
allCardsContainer = document.createElement('div');

allCardsContainer.className = 'all-cards-container';

const mountReviews = results => {
  const newReviewCards = results.map(resultData => new ReviewCard(resultData));
  newReviewCards.forEach(ReviewCard => allCardsContainer.appendChild(ReviewCard.mountOneCard()));
  reviewCards = [...reviewCards, ...newReviewCards]
}
  const layAllCards = results => {
    root.innerHTML = '';
    root.appendChild(criticsPickCheckbox);
    checkIsChecked();
    root.appendChild(allCardsContainer);
    mountReviews(results);
    setTimeout(() => allCardsContainer.classList.add('all-cards-container--show'), 0);
  }

const stopScroll = () => {
  const noMoreResultsMsg = document.createElement('div');
  noMoreResultsMsg.className = 'no-more-results-msg';
  noMoreResultsMsg.innerHTML = 'No more reviews';
  root.appendChild(noMoreResultsMsg);
}

const loadMoreReviewsOnScroll = () => {
  api.getReviews(results => {
    let constinueScroll = true;
    if(results.length < portion) constinueScroll = false;
    mountReviews(results);
    fetchCycles++;
    if(constinueScroll) window.addEventListener('scroll', checkPageScroll);
    else stopScroll;
  }, portion * fetchCycles)
}

const checkPageScroll = () => {
  if(window.scrollY >= document.documentElement.scrollHeight - 2 * window.innerHeight) {
    loadMoreReviewsOnScroll();
    window.removeEventListener('scroll', checkPageScroll)
  }
}

api.getReviews(layAllCards);

window.addEventListener('scroll', checkPageScroll);