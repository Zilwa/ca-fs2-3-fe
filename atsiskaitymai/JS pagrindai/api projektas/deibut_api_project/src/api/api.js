const getReviews = (callback, offset) => {
  const url = `https://api.nytimes.com/svc/movies/v2/reviews/all.json?api-key=89xmBEkLWAwobu09nVug9fdZEyHjTKu4${offset ? '&offset='+offset : ''}`;
  const options = {
    method: "GET",
    headers: {
      "Accept": "movieReviewsNyt"
    },
  };
  fetch(url, options) 
    .then(response => response.json())
    .then(data => {
      if(callback) {
        callback(data.results)
      }
    })
    .catch(err => console.error(err));
}

export default {
  getReviews
}

// 29465 reviews