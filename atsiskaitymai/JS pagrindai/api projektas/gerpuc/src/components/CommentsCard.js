import defaultImg from '../assets/img/no-user-img.png';


export default class CommentsCard {
    static requiredProps = [
      'user',
      'text',
      'created_at_ago',
      'num_likes',
      'images'
      
     
    ];

    static imageSizes = {
        thumb: 40,
        small: 150,
        medium: 350,
        large: 700,
      }

      constructor(props) {
        this.element = document.createElement('article');
        this.element.className = 'user-card';

        CommentsCard.requiredProps.forEach(prop => this[prop] = props[prop] || '');   
      }  

    mount() {
      
        const img = typeof this.user.imageUrl === 'string' ? this.user.imageUrl : defaultImg;
        const userName = typeof this.user.username === 'string' ? this.user.username : '';
        this.element.innerHTML = `
      <div class= "user-card__userName">
        <div>
          <img 
          class="user-card__img" src="${img}" 
          height="${CommentsCard.imageSizes.thumb}px" 
          width="${CommentsCard.imageSizes.thumb}px"
        />
        </div>
        <div>
          <h2> ${userName}</h2>
        </div>
    </div>
        
        <p class="date_created"> Created at: ${this.created_at_ago} </p>
      <hr />
        <h3>${this.text} </h3>
       
        <p class= "like_numbers">  <i class="fa fa-thumbs-up"></i>  ${this.num_likes} </p>
        ${this.images}
        `;
        return this.element;
    }  
  }
  
  

  
