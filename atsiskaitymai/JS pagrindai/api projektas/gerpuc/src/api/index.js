//https://www.boardgameatlas.com/api/docs/users

// https://api.boardgameatlas.com/api/search?name=Catan&client_id=JLBr5npPhV

//https://www.boardgameatlas.com/api/forum/comments?client_id=JLBr5npPhV 
// forum comments


const getUsers = (callback, from) => {
    fetch(`https://www.boardgameatlas.com/api/forum/comments?client_id=JLBr5npPhV${from ? '&skip=' + from : ''}`)
    .then(response => response.json())
    .then(data => callback(data.comments))
    .catch(error => console.error(
        `klaida ${error}`
    ))
}

export default {
    getUsers
}