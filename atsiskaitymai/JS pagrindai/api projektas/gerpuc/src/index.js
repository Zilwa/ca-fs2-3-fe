
import api from './api';
import loadingGif from './assets/img/loading.gif';

import CommentsCard from './components/CommentsCard';
import './scss/main.scss'


const root = document.querySelector('#app'),
userFetchCount = 20;

let userCards,
segment = 1,
cardContainer;
// inputElement;

const loadingAnimation = document.createElement('div');
loadingAnimation.className = 'loading-animation';
loadingAnimation.innerHTML = `<img src="${loadingGif}">`;

const mountUsers = () => {
    root.innerHTML = '';
    cardContainer = document.createElement('div');
    cardContainer.className = 'card-container';
    root.appendChild(cardContainer);
  
    userCards.forEach(userCard => cardContainer.appendChild(userCard.mount()));
    setTimeout(() => cardContainer.classList.add('show'), 0);
}

const loadUsers = () => {
  root.appendChild(loadingAnimation);
  api.getUsers((users) => {
    users = users.slice(0, userFetchCount);
    //aptazinti duomenu pasibaigima

    let continueInfiniteScroll = true;
    if(users.length < userFetchCount) continueInfiniteScroll = false;

    const newUserCards = users.map(user => new CommentsCard(user));
    newUserCards.forEach(userCard => cardContainer.appendChild(userCard.mount()));
    userCards = [...userCards, ...newUserCards];
    root.removeChild(loadingAnimation);
    segment++;
    
    if(continueInfiniteScroll) window.addEventListener('scroll', checkPageScroll);    
    
  }, segment * userFetchCount ); 
}

const checkPageScroll = () => {
    if (window.scrollY >= document.documentElement.scrollHeight - 2 * window.innerHeight) {
      loadUsers();
      window.removeEventListener('scroll', checkPageScroll);
    }
  }
  

root.appendChild(loadingAnimation);

api.getUsers((users) => {

    users = users.slice(0, userFetchCount);

    userCards = users.map(userData => new CommentsCard(userData));
    mountUsers();

});

window.addEventListener('scroll', checkPageScroll);


