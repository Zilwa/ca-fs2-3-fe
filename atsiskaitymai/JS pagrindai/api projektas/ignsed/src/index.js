import BreweryCard from './components/BreweryCard';
import api from './api';
import './scss/main.scss';

const 
root = document.querySelector('#app');

let
breweryCards = [],
cardContainer;

cardContainer = document.createElement('div');
cardContainer.className = 'card-container';

const initBreweryLoad = breweries => {
  root.appendChild(cardContainer);
  mountBreweries(breweries);
  setTimeout(() => cardContainer.classList.add('show'), 0);
}

const mountBreweries = breweries => {
  const newBreweryCards = breweries.map(breweryData => new BreweryCard(breweryData));
  newBreweryCards.forEach(breweryCard => cardContainer.appendChild(breweryCard.mount()));
  breweryCards = [...breweryCards, ...newBreweryCards];
}

api.getBreweries(initBreweryLoad);


      