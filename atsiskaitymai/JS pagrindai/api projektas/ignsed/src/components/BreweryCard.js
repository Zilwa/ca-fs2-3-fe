export default class BreweryCard {
    static propertiesForCards = [
        'name',
        'city',
        'state',
        'street',
        'phone',
        'adress',
        'created_at',
        'website_url'
    ];

    constructor(properties) {
        this.element = document.createElement('article');
        this.element.className = 'brewery-card';
        BreweryCard.propertiesForCards.forEach(property => this[property] = properties[property] || '');
    }

    mount() {
        this.element.innerHTML = `
      <h2 class="brewery-card__header">
      <a href="#" target="blank">${this.name}</a>
      </h2>
      <h4 class="brewery-card__header__adress">This brewery is located in ${this.street} street, ${this.city} city, ${this.state}.</h4>
      <p>Call us anytime: 📞${this.phone}, or visit <a href="${this.website_url}">${this.name}</a> for more information</p>
    `;
        return this.element;
    }
}
