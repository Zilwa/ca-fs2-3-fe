const getBreweries = (callback) => {
  fetch('https://api.openbrewerydb.org/breweries')
  .then(response => response.json())
  .then(data => callback(data));
}

export default {
  getBreweries
}