//Replace Text function		 
$(function () { 
    count = 0; 
    wordsArray = ["CSS", "JavaScript", "PHP", "SCSS", "HTML"]; 
    setInterval(function () { 
      count++; 
      $("#word").fadeOut(500, function () { 
        $(this).text(wordsArray[count % wordsArray.length]).fadeIn(500); 
      }); 
    }, 3500); 
  }); 
  //End Replace Text function	