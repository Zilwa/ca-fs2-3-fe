a. Sugalvoti puslapio temą ir dizainą
  - Puslapio tema/tikslas?
  - Sekcijos, jų kiekis, paskirtis.
  - Puslapio/sekcijų komponentai, jų paskirtis.
  - Sudėtinių komponentų(pvz. koretelės) dizaino eskizai visiems ekrano dydžiams.
  - Sekcijų dizaino eskizai visiems ekrano dydžiams.
  - Darbų eiliškumo planas.

b. Padaryti puslapį, kuris korektiškai atvaizduotų turinį pagal ekrano dydį [5 ekrano dydžiai].

Puslapio vertinimo kriterijai:
  1. Tvarkinga puslapio failų struktūra. Failai turi būti skirstomi pagal jūsų sugalvotą logiką, temas ir/ar paskirtį. [1]
  2. Naršymo meniu viršuje. Mažėjant ekrano pločiui turi pakisti navigacijos meniu vaizdas (pvz. pasislepiamos nuorodos). [1]
  3. Bent 3 puslapio sekcijos. [1]
  4. BEM standarto laikymasis kuriant sudėtinius komponentus. [1]
  5. Bent 2 sudėtiniai komponentai su skirtingu dizainu visiems ekrano dydžiams. [2]
  6. SCSS kintamųjų naudojimas. [1]
  7. Grid 'template-areas' ir/arba 'grid lines' ir/arba 'Bootstrap grid system' panaudojimas. [1]
  8. Flex panaudojimas. [1]
  9. Turinį sufleruojančių HTML elementų panaudojimas: [1]
    - header
    - nav
    - footer
    - section
    - main
    - aside
    - h1-h6
    - article